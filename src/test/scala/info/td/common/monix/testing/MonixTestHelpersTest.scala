package info.td.common.monix.testing

import info.td.common.monix.MonixHelpers._
import monifu.concurrent.schedulers.TestScheduler
import utest._
import utest.framework.TestSuite

import scala.concurrent.duration._

object MonixTestHelpersTest extends TestSuite {

  val tests = TestSuite {

    val scheduler = TestScheduler()

    "delaysBetweenEmissions" - {
      val delays = Seq(100.milli, 100.milli, 200.milli, 300.milli)
      val totalDelay = delays.reduceLeft(_ + _)
      val start = scheduler.now
      val result = MonixTestHelpers
        .delaysBetweenEmissions(delays)
        .map((_, scheduler.now - start))
        .toVectorWithAction(totalDelay) {
          scheduler.advanceTimeBy(totalDelay)
        }(scheduler)
      val expectedResult = Vector(
        (0, 100),
        (1, 200),
        (2, 400),
        (3, 700)
      )
      assert(result == expectedResult)
    }
  }
}
