package info.td.common.monix

import java.util.Date

import info.td.common.monix.MonixHelpers._
import info.td.common.monix.testing.MonixTestHelpers
import monifu.concurrent.Scheduler
import monifu.reactive.{Ack, Observable}
import utest._
import utest.framework.TestSuite

import scala.concurrent.duration._
import scala.util.Random


object ProducerConsumerTest extends TestSuite {
  val tests = TestSuite {
    object GetByOneProducerConsumerStrategy extends ProducerConsumerStrategy[Int, Int] {
      type StateType = List[Int]

      override def add(elements: StateType, element: Int) = elements :+ element

      override def get(elements: StateType) = {
        elements match {
          case element :: tail ⇒ (Some(element), tail)
          case Nil ⇒ (None, Nil)
        }
      }

      def empty = Nil
    }

    object GetAllProducerConsumerStrategy extends ProducerConsumerStrategy[Int, List[Int]] {
      type StateType = List[Int]

      override def add(elements: StateType, element: Int) = elements :+ element

      override def get(elements: StateType) = {
        if (elements.isEmpty) {
          (None, Nil)
        } else {
          (Some(elements), Nil)
        }
      }

      override def empty = Nil
    }

    val error: RuntimeException = new RuntimeException("Test error")

    object ErrorWhenAddingProducerConsumerStrategy extends ProducerConsumerStrategy[Int, Any] {
      type StateType = Any

      override def add(elements: StateType, element: Int) = {
        throw error
      }

      override def get(state: StateType) = (None, ())

      override def empty: Unit = ()
    }

    object ErrorWhenGettingProducerConsumerStrategy extends ProducerConsumerStrategy[Int, Any] {
      type StateType = Any

      override def add(elements: StateType, element: Int) = ()

      override def get(state: StateType) = {
        throw error
      }

      override def empty: Unit = ()
    }

    "getByOne" - MonixTestHelpers.testWithSchedulers { scheduler ⇒
      val timeSlice = 20.milli

      val result: Vector[Int] = MonixTestHelpers
        .delaysBetweenEmissions(Seq(100.milli, 100.milli, 200.milli))
        .map(_.toInt)
        .producerConsumer(GetByOneProducerConsumerStrategy)
        .doWork(_ ⇒ scheduler.advanceTimeBy(timeSlice * 3 + timeSlice / 5))
        .toVectorWithAction(timeSlice * 15) {
          scheduler.advanceTimeBy(timeSlice * 15)
        }(scheduler)

      val expectedResult = Vector(0, 1, 2)
      assert(result == expectedResult)
    }

    "getAll" - MonixTestHelpers.testWithSchedulers { scheduler ⇒
      val timeSlice = 20.milli

      val result: Vector[List[Int]] =
        Observable
          .intervalAtFixedRate(timeSlice)
          .map(_.toInt)
          .take(10)
          .producerConsumer(GetAllProducerConsumerStrategy)
          .doWork(_ ⇒ scheduler.advanceTimeBy(timeSlice * 3 + timeSlice / 5))
          .toVectorWithAction(timeSlice * 15) {
            scheduler.advanceTimeBy(timeSlice * 15)
          }(scheduler)

      assert(result.size == 4)
      assert(result(0) == List(0))
      assert(result(1) == List(1, 2, 3))
      assert(result(2) == List(4, 5, 6))
      assert(result(3) == List(7, 8, 9))
    }

    "getAll many elements slow subscriber" - MonixTestHelpers.testWithSchedulers { scheduler ⇒
      val timeSlice = 2
      val numberOfElements = 1000
      val r = new Random(new Date().getTime)
      val xsList =
        MonixTestHelpers
          .delaysBetweenEmissions(Seq.tabulate(numberOfElements)(_ ⇒ r.nextInt(timeSlice).milli))
          .map(_.toInt)
          .producerConsumer(GetAllProducerConsumerStrategy)
          .doWork((_, scheduler.advanceTimeBy(r.nextInt(timeSlice * 3).milli)))
          .toVectorWithAction((numberOfElements * timeSlice).milli) {
            scheduler.advanceTimeBy(100.milli)
          }(scheduler)

      val flattenedResult = xsList.flatten
      val expectedResult = (0 until numberOfElements).toVector
      assert(flattenedResult == expectedResult)
    }

    "getAll many elements fast subscriber" - MonixTestHelpers.testWithSchedulers { scheduler ⇒
      val timeSlice = 2
      val numberOfElements = 1000
      val r = new Random(new Date().getTime)
      val xsList =
        MonixTestHelpers
          .delaysBetweenEmissions(Seq.tabulate(numberOfElements)(_ ⇒ r.nextInt(timeSlice).milli))
          .map(_.toInt)
          .producerConsumer(GetAllProducerConsumerStrategy)
          .toVectorWithAction((numberOfElements * timeSlice).milli) {
            scheduler.advanceTimeBy((numberOfElements * timeSlice).milli)
          }(scheduler)

      val flattenedResult = xsList.flatten
      val expectedResult = (0 until numberOfElements).toVector
      assert(flattenedResult == expectedResult)
    }

    def assertErrorEmitted[T](o: Observable[T], time: FiniteDuration)(implicit scheduler: Scheduler): Unit = {
      var errorThrown: Throwable = null
      o.subscribe(_ ⇒ Ack.Continue, error ⇒ errorThrown = error)
      scheduler.advanceTimeBy(time)
      assert(errorThrown == error)
    }

    "error producing ProducerConsumer" - MonixTestHelpers.testWithSchedulers { schedulerProvider ⇒
      assertErrorEmitted(Observable
        .from(1, 2, 3)
        .producerConsumer(ErrorWhenAddingProducerConsumerStrategy),
        100.milli
      )(schedulerProvider)
    }

    "get throwing an error is handled correctly " - MonixTestHelpers.testWithSchedulers { schedulerProvider ⇒
      assertErrorEmitted(Observable
        .from(1, 2, 3)
        .producerConsumer(ErrorWhenGettingProducerConsumerStrategy),
        100.milli
      )(schedulerProvider)
    }
  }
}
