package info.td.common.monix

import java.util.Date

import info.td.common.monix.MonixHelpers._
import info.td.common.monix.testing.MonixTestHelpers
import monifu.reactive.Observable
import utest._
import utest.framework.TestSuite

import scala.concurrent.duration._
import scala.util.Random

object BufferIntrospectiveTest extends TestSuite {

  val tests = TestSuite {
    "BufferIntrospective" - MonixTestHelpers.testWithSchedulers { scheduler ⇒
      val timeSlice = 20.milli
      val xsList =
        Observable
          .intervalAtFixedRate(timeSlice)
          .take(10)
          .bufferIntrospective
          .doWork(_ ⇒ scheduler.advanceTimeBy(timeSlice * 3 + timeSlice / 5))
          .toVectorWithAction(timeSlice * 15) {
            scheduler.advanceTimeBy(timeSlice * 15)
          }(scheduler)
          .map(_.toList)

      assert(xsList.size == 4)
      assert(xsList.head == List(0))
      assert(xsList(1) == List(1, 2, 3))
      assert(xsList(2) == List(4, 5, 6))
      assert(xsList(3) == List(7, 8, 9))
    }

    "BufferIntrospective many elements, fast subscriber" - MonixTestHelpers.testWithSchedulers { scheduler ⇒
      val timeSlice = 2
      val numberOfElements = 1000
      val r = new Random(new Date().getTime)
      val xsList =
        MonixTestHelpers
          .delaysBetweenEmissions(Seq.tabulate(numberOfElements)(_ ⇒ r.nextInt(timeSlice).milli))
          .bufferIntrospective
          .toVectorWithAction((numberOfElements * timeSlice).milli) {
            scheduler.advanceTimeBy(numberOfElements * timeSlice.milli / 2)
          }(scheduler)

      val flattenedResult = xsList.flatten
      val expectedResult = (0 until numberOfElements).toVector
      assert(flattenedResult == expectedResult)
    }

    "BufferIntrospective many elements, slow subscriber" - MonixTestHelpers.testWithSchedulers { scheduler ⇒
      val timeSlice = 2
      val numberOfElements = 1000
      val r = new Random(new Date().getTime)
      val xsList =
        MonixTestHelpers
          .delaysBetweenEmissions(Seq.tabulate(numberOfElements)(_ ⇒ r.nextInt(timeSlice).milli))
          .bufferIntrospective
          .doWork((_, scheduler.advanceTimeBy(r.nextInt(timeSlice * 3).milli)))
          .toVectorWithAction((numberOfElements * timeSlice).milli) {
            scheduler.advanceTimeBy(100.milli)
          }(scheduler)

      val flattenedResult = xsList.flatten
      val expectedResult = (0 until numberOfElements).toVector
      assert(flattenedResult == expectedResult)
    }
  }
}