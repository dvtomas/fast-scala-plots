package info.td.common.javafx

import javafx.application.Application
import javafx.stage.Stage

import scala.concurrent.duration._
import scala.concurrent.{Await, Promise}

object JavaFXToolkitLauncher {
  private[this] val toolkitStartedPromise = Promise[Unit]()

  private[this] class DummyJavaFXApp extends Application {
    def start(stage: Stage) = {
      toolkitStartedPromise.success(())
    }
  }

  private[this] lazy val javaFXThread = {
    val thread = new Thread("JavaFX Init Thread") {
      override def run(): Unit = {
        Application.launch(classOf[DummyJavaFXApp], "")
      }
    }
    thread.setDaemon(true)
    thread.start()
    Await.result(toolkitStartedPromise.future, 1.second)
    thread
  }

  def launchToolkit(): Unit = {
    javaFXThread
  }
}



















