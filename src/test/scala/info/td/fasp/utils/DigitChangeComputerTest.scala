package info.td.fasp.utils

import utest._
import utest.framework.TestSuite

object DigitChangeComputerTest extends utest.TestSuite {
  private def digitChange(a: Double, b: Double): DigitChange = new DigitChangeComputer {
    override protected def maximumDigitsToCheck: Int = 3
  }.digitChange(a, b)

  private[this] def assertClose(actual: Double, expected: Double): Unit = {
    assert((actual - expected).abs < actual.abs / 10000.0)
  }

  private[this] def assertDigitChange(a: Double, b: Double, expectedValue: Double, expectedMagnitudeChange: Int) {
    val DigitChange(value, magnitudeChange, _) = digitChange(a, b)
    assertClose(value, expectedValue)
    assert(magnitudeChange == expectedMagnitudeChange)
  }

  private def assertDigitChanges(a: Double, b: Double, expectedValue: Double, expectedMagnitudeChange: Int) {
    assertDigitChange(a, b, expectedValue, expectedMagnitudeChange)
    assertDigitChange(b, a, expectedValue, expectedMagnitudeChange)
    assertDigitChange(-a, -b, -expectedValue, expectedMagnitudeChange)
    assertDigitChange(-b, -a, -expectedValue, expectedMagnitudeChange)
  }


  val tests = TestSuite {
    "DigitChangeComputer" - {
      "return zero when one value is positive and the other negative" - {
        assert(digitChange(-3.5, 1.0).value == 0.0)
        assert(digitChange(-3.5, 1.0).value == 0.0)
        assert(digitChange(0.0, 0.0).value == 0.0)
      }

      "return minimumOrderToCheck - 1 set in the MagnitudeChangeComputer when the values are equal" - {
        assert(digitChange(1.0, 1.0) == DigitChange(1.0, -4, None))
        assert(digitChange(-23.1, -23.1) == DigitChange(-23.1, -4, None))
      }

      "..." - {
        assertDigitChanges(1.9, 2.1, 2.0, 0)
        assertDigitChanges(150.0, 250.0, 200.0, 2)
        assertDigitChanges(0.015, 0.025, 0.02, -2)
        assertDigitChanges(1234.5, 1244.5, 1240.0, 1)
        assertDigitChanges(0.12345, 0.12445, 0.1240, -3)
        assertDigitChanges(0.123456789, 0.123456889, 0.123, -4) // currentExponent smaller than minimum indicates that the order change has not been found
        assertDigitChanges(1.23e100, 1.33e100, 1.3e100, 99)
        assertDigitChanges(1.23e-100, 1.33e-100, 1.3e-100, -101)
        assertDigitChanges(1.835e-100, 1.845e-100, 1.84e-100, -102)
        assertDigitChanges(7.012, 7.022, 7.02, -2) // must not get confused by zeroes in the middle of the number
      }
    }
  }
}


