package info.td.fasp.controller

import info.td.fasp.axis.{LinearAxisX, LinearAxisY}
import info.td.fasp.figurepart.{Axes, FigurePartKey}
import info.td.fasp.geom.{DataRange, ScreenRange, ScreenRectangle}
import monifu.reactive.subjects.PublishSubject
import utest._
import utest.framework.TestSuite

import scala.concurrent.Await

object GenericFigureControllerTest extends TestSuite {

  object TestingPlotController {

    object Grabbed

    sealed abstract class InputEvent extends InputPlotControllerEvent

    case class Grab(symbolKey: Symbol) extends InputEvent {
      override def isFor(axesOnScreen: AxesOnScreen): Boolean = axesOnScreen.key.key == symbolKey
    }

    case object Release extends InputEvent {
      override def isFor(axesOnScreen: AxesOnScreen): Boolean = true
    }

    case class ChangeValue(symbolKey: Symbol, value: Int) extends InputEvent {
      override def isFor(axesOnScreen: AxesOnScreen): Boolean = axesOnScreen.key.key == symbolKey
    }

    sealed abstract class OutputEvent extends OutputPlotControllerEvent

    case class ChangedValue(symbolKey: Symbol, value: Int) extends OutputEvent

  }

  class TestingPlotController extends AbstractPlotController {

    import TestingPlotController._

    override type State = TestingPlotController.Grabbed.type
    override type InputEvent = TestingPlotController.InputEvent
    override type OutputEvent = TestingPlotController.OutputEvent

    @volatile var lastAxesOnScreen: AxesOnScreen = null
    @volatile var lastEvent: InputEvent = null

    override def handleNewAxesOnScreen(
                                        state: State,
                                        oldAxesOnScreen: AxesOnScreen,
                                        newAxesOnScreen: AxesOnScreen
                                      ): (Option[State], AxesOnScreen) = {
      if (oldAxesOnScreen.axesScreenBounds == newAxesOnScreen.axesScreenBounds) {
        (Some(state), newAxesOnScreen)
      } else {
        (Some(state), oldAxesOnScreen)
      }
    }

    override def handleControllerEvent(
                                        axesOnScreen: AxesOnScreen,
                                        maybeState: Option[State],
                                        controllerEvent: InputEvent
                                      ): (Option[State], Option[OutputEvent]) = {
      lastAxesOnScreen = axesOnScreen
      lastEvent = controllerEvent

      controllerEvent match {
        case Grab(_) ⇒ (Some(Grabbed), None)
        case Release ⇒ (None, None)
        case setDataRanges@ChangeValue(_, value) ⇒ (
          maybeState,
          Some(ChangedValue(axesOnScreen.key.key, value))
          )
      }
    }
  }

  // Isn't entirely deterministic for unknown reason :( Fails sometimes.
  val tests = TestSuite {
    import TestingPlotController._

    implicit val scheduler = monifu.concurrent.Implicits.globalScheduler
    val testingPlotController = new TestingPlotController
    val figureController = new GenericFigureController[TestingPlotController](testingPlotController)

    val controllerEventSubject = PublishSubject[TestingPlotController#InputEvent]
    val newAxesCollectionSubject = PublishSubject[Seq[AxesOnScreen]]

    @volatile var lastChangedValue: OutputPlotControllerEvent = null
    @volatile var lastMaybePlotControllerState: Option[TestingPlotController#State] = null

    figureController.outputs.outputPlotControllerEventSource.foreach(lastChangedValue = _)
    figureController.outputs.maybePlotControllerStateSource.foreach(lastMaybePlotControllerState = _)
    figureController.subscribe(controllerEventSubject, newAxesCollectionSubject)

    val testingAxes1 = AxesOnScreen(FigurePartKey('axes1), ScreenRectangle.topLeftBottomRight(0, 0, 10, 10),
      Axes(
        LinearAxisX(ScreenRange(2, 8), DataRange(0.0, 1.0)),
        LinearAxisY(ScreenRange(2, 8), DataRange(0.0, 1.0))
      ))

    val testingAxes2 = AxesOnScreen(FigurePartKey('axes2), ScreenRectangle.topLeftBottomRight(10, 0, 20, 10),
      Axes(
        LinearAxisX(ScreenRange(2, 8), DataRange(0.0, 1.0)),
        LinearAxisY(ScreenRange(12, 18), DataRange(0.0, 1.0))
      ))

    def assertLastFigureControllerOutput(
                                          expectedLastMaybePlotControllerState: Option[TestingPlotController#State],
                                          expectedLastSetDataRanges: ChangedValue
                                        ): Unit = {
      eventually(lastMaybePlotControllerState == expectedLastMaybePlotControllerState)
      eventually(lastChangedValue == expectedLastSetDataRanges)
    }

    import concurrent.duration._
    implicit val retryMax = utest.asserts.RetryMax(3000.millis)

    def assertLastPlotControllerInput(expectedLastFigurePartKey: Symbol, expectedLastControllerEvent: TestingPlotController.InputEvent): Unit = {
      eventually {
        val actualKey = if (testingPlotController.lastAxesOnScreen == null) null else testingPlotController.lastAxesOnScreen.key
        actualKey.key == expectedLastFigurePartKey
      }
      eventually(testingPlotController.lastEvent == expectedLastControllerEvent)
    }

    def pushEvent(event: TestingPlotController.InputEvent): Unit = {
      import concurrent.duration._
      Await.ready(controllerEventSubject.onNext(event), 100.milli)
    }

    def pushNewAxes(axesOnScreenCollection: Seq[AxesOnScreen]): Unit = {
      import concurrent.duration._
      Await.ready(newAxesCollectionSubject.onNext(axesOnScreenCollection), 100.milli)
    }

    "Controller events outside plots don't do anything " - {
      pushNewAxes(Seq(testingAxes1, testingAxes2))
      pushEvent(TestingPlotController.Grab('badSymbol))
      assertLastPlotControllerInput(null, null)
      assertLastFigureControllerOutput(None, null)
    }

    "handleControllerEvent" - {
      pushNewAxes(Seq(testingAxes1, testingAxes2))
      // Grab Axes 1
      pushEvent(TestingPlotController.Grab('axes1))
      assertLastPlotControllerInput('axes1, TestingPlotController.Grab('axes1))
      assertLastFigureControllerOutput(Some(TestingPlotController.Grabbed), null)

      // SetDataRanges on axes 1
      pushEvent(TestingPlotController.ChangeValue('axes1, 2))
      assertLastPlotControllerInput('axes1, TestingPlotController.ChangeValue('axes1, 2))
      assertLastFigureControllerOutput(Some(TestingPlotController.Grabbed), ChangedValue('axes1, 2))

      // SetDataRanges on axes 1 again
      pushEvent(TestingPlotController.ChangeValue('axes1, 3))
      assertLastPlotControllerInput('axes1, TestingPlotController.ChangeValue('axes1, 3))
      assertLastFigureControllerOutput(Some(TestingPlotController.Grabbed), ChangedValue('axes1, 3))

      // Try SetDataRanges on axes 2 - It will be accepted, when a figure part is grabbed, all events are delivered to it even when they don't belong to it. This is so that when e.g drag was activated, we want to continue dragging even when the cursor is outside the plot area.
      pushEvent(TestingPlotController.ChangeValue('axes2, 4))
      assertLastPlotControllerInput('axes1, TestingPlotController.ChangeValue('axes2, 4))
      assertLastFigureControllerOutput(Some(TestingPlotController.Grabbed), ChangedValue('axes1, 4))

      // Release axes 1
      pushEvent(TestingPlotController.Release)
      assertLastPlotControllerInput('axes1, TestingPlotController.Release)
      assertLastFigureControllerOutput(None, ChangedValue('axes1, 4))

      // Try SetDataRanges on axes 2 again
      pushEvent(TestingPlotController.ChangeValue('axes2, 5))
      assertLastPlotControllerInput('axes2, TestingPlotController.ChangeValue('axes2, 5))
      assertLastFigureControllerOutput(None, ChangedValue('axes2, 5))
    }

    "handleNewAxesOnScreen" - {
      pushNewAxes(Seq(testingAxes1, testingAxes2))

      val modifiedTestingAxes1 = testingAxes1.copy(axes = testingAxes1.axes.copy(axisX = LinearAxisX(ScreenRange(2, 8), DataRange(0.0, 2.0))))

      // No axis is grabbed now, this should always be performed
      pushNewAxes(Seq(modifiedTestingAxes1, testingAxes2))

      pushEvent(TestingPlotController.Grab('axes1))
      assert(testingPlotController.lastAxesOnScreen.axes == modifiedTestingAxes1.axes)

      // SetDataRanges on axes 1
      pushEvent(TestingPlotController.ChangeValue('axes1, 1))
      assert(testingPlotController.lastAxesOnScreen.axes == modifiedTestingAxes1.axes)

      // Change axes. Testing handleNewAxesOnScreen is implemented in such a way, that it will accept modifiedTestingAxes2, because the axesScreenBounds have not changed.
      val modifiedTestingAxes2 = testingAxes1.copy(axes = testingAxes1.axes.copy(axisX = LinearAxisX(ScreenRange(2, 8), DataRange(0.0, 3.0))))
      pushNewAxes(Seq(modifiedTestingAxes2, testingAxes2))
      pushEvent(TestingPlotController.ChangeValue('axes1, 2))
      assertLastFigureControllerOutput(Some(TestingPlotController.Grabbed), ChangedValue('axes1, 2))
      assert(testingPlotController.lastAxesOnScreen.axes == modifiedTestingAxes2.axes)

      // Change axes again. Testing handleNewAxesOnScreen is implemented in such a way, that it will NOT accept modifiedTestingAxes3, because the axesScreenBounds have changed.
      val modifiedTestingAxes3 = testingAxes1.copy(axes = testingAxes1.axes.copy(axisX = LinearAxisX(ScreenRange(2, 10), DataRange(0.0, 3.0))))
      pushNewAxes(Seq(modifiedTestingAxes3, testingAxes2))
      pushEvent(TestingPlotController.ChangeValue('axes1, 3))
      assertLastFigureControllerOutput(Some(TestingPlotController.Grabbed), ChangedValue('axes1, 3))
      assert(testingPlotController.lastAxesOnScreen.axes == modifiedTestingAxes2.axes)

      // Make sure that axes 1 is grabbed by asserting that an action on axes2 is ignored
      pushEvent(TestingPlotController.ChangeValue('axes2, 3))
      assertLastFigureControllerOutput(Some(TestingPlotController.Grabbed), ChangedValue('axes1, 3))
      assert(testingPlotController.lastAxesOnScreen.axes == modifiedTestingAxes2.axes)

      // Remove axes1 altogether, and assert that it has been properly released by trying out an action on axes 2
      pushNewAxes(Seq(testingAxes2))
      pushEvent(TestingPlotController.ChangeValue('axes2, 3))
      assertLastFigureControllerOutput(None, ChangedValue('axes2, 3))
      assert(testingPlotController.lastAxesOnScreen.axes == testingAxes2.axes)
    }
  }
}