package info.td.common.monix.javafx

import javafx.application.Platform
import javafx.beans.property.{ReadOnlyObjectProperty, ReadOnlyObjectWrapper}
import javafx.beans.value.{ChangeListener, ObservableValue}
import javafx.event.{Event, EventHandler, EventType}
import javafx.scene.Node

import monifu.concurrent.Scheduler
import monifu.reactive.channels.PublishChannel
import monifu.reactive.{Ack, Observable, OverflowStrategy}

import scala.concurrent.{Future, Promise}

object JavaFXObservable {
  def subscriptionOnJavaFXApplicationThread(action: ⇒ Unit): Future[Ack] = {
    val promise = Promise[Ack]()
    Platform.runLater(() ⇒ {
      action
      promise.success(Ack.Continue)
    })
    promise.future
  }

  def futureOnJavaFXApplicationThread[T](action: ⇒ T): Future[T] = {
    val promise = Promise[T]()
    Platform.runLater(() ⇒ promise.success(action))
    promise.future
  }

  def fromNodeEvents[T <: Event](source: Node, eventType: EventType[T])(implicit scheduler: Scheduler): Observable[T] = {
    val channel = PublishChannel[T](OverflowStrategy.DropOld(256))

    val handler = new EventHandler[T] {
      override def handle(event: T): Unit = channel pushNext event
    }

    Observable.create[T] { subscriber ⇒
      channel.subscribe(subscriber)
      source.addEventHandler(eventType, handler)
    }.doOnCanceled {
      Platform.runLater(() ⇒ source.removeEventHandler(eventType, handler))
    }
  }

  def fromObservableValue[T](fxObservable: ObservableValue[T])(implicit scheduler: Scheduler): Observable[T] = {
    val channel = PublishChannel[T](OverflowStrategy.DropOld(256))
    val changeListener = new ChangeListener[T] {
      def changed(observableValue: ObservableValue[_ <: T], previous: T, current: T) = {
        channel pushNext current
      }
    }

    Observable.create[T] { subscriber ⇒
      channel.subscribe(subscriber)
      fxObservable.addListener(changeListener)
    }.doOnCanceled {
      Platform.runLater(() ⇒ fxObservable.removeListener(changeListener))
    }
  }

  def observableToProperty[T](observable: Observable[T])(implicit scheduler: Scheduler): ReadOnlyObjectProperty[T] = {
    val propertyWrapper = new ReadOnlyObjectWrapper[T]()
    observable.subscribe(next ⇒ subscriptionOnJavaFXApplicationThread {
      propertyWrapper.set(next)
    })
    propertyWrapper.getReadOnlyProperty
  }
}