package info.td.common.monix

import info.td.common.logging.TraceHelpers.{DumpFunction, printlnDumpFunction}
import info.td.common.monix.impl.{BufferIntrospectiveSubscriber, ProducerConsumerSubscriber}
import monifu.concurrent.Scheduler
import monifu.concurrent.schedulers.TestScheduler
import monifu.reactive.Observable
import rx.RxReactiveStreams

import scala.concurrent.duration.{Duration, FiniteDuration}

trait MonixObservableExtensions {
  def combineLatestSeq[T](observables: Seq[Observable[T]]): Observable[Seq[T]] =
    Observable.combineLatestList(observables: _*)

  implicit class ObservableWrapper[T](observable: Observable[T]) {
    def bufferIntrospective: Observable[Seq[T]] = {
      Observable.create { subscriber ⇒
        val introspectiveSubscriber = new BufferIntrospectiveSubscriber[T](subscriber, 0)
        observable.onSubscribe(introspectiveSubscriber)
      }
    }

    def observeLatest: Observable[T] =
      bufferIntrospective.map(_.last)

    def producerConsumer[R](producerConsumerStrategy: ProducerConsumerStrategy[T, R]): Observable[R] = {
      Observable.create { subscriber ⇒
        val introspectiveSubscriber = new ProducerConsumerSubscriber[T, R](subscriber, producerConsumerStrategy)
        observable.onSubscribe(introspectiveSubscriber)
      }
    }

    def mergeMapIterable[R](getIterableFunc: T ⇒ Iterable[R]): Observable[R] = {
      observable.mergeMap(each ⇒ Observable fromIterable getIterableFunc(each))
    }

    def concatMapIterable[R](getIterableFunc: T ⇒ Iterable[R]): Observable[R] = {
      observable.concatMap(each ⇒ Observable fromIterable getIterableFunc(each))
    }

    def mapOption[R](getOptionFunc: T ⇒ Option[R]): Observable[R] = {
      observable
        .map(getOptionFunc)
        .collect { case Some(value) ⇒ value }
    }

    /**
      * For each internal state and an incoming element generate a new state and produce a new output element.
      * The resulting observable consists of the produced output elements only. The state is internal to the operator.
      */
    def scanResults[S, R](initialState: S)(f: (S, T) ⇒ (S, R)): Observable[R] =
      scanMaybeResults(initialState) { case (state, t) ⇒
        val (newState, result) = f(state, t)
        (newState, Some(result))
      }

    /**
      * For each internal state and an incoming element generate a new state, and maybe produce a new output element.
      * The resulting observable consists of the produced output elements only. The state is internal to the operator.
      */
    def scanMaybeResults[S, R](initialState: S)(f: (S, T) ⇒ (S, Option[R])): Observable[R] = {
      type StateWithResult = (S, Option[R])
      observable
        .scan[StateWithResult]((initialState, None)) { case (((state, _), t)) ⇒ f(state, t) }
        .collect { case (_, Some(value)) ⇒ value }
    }

    def toVector(timeout: Duration)(implicit scheduler: Scheduler): Vector[T] =
      impl.blocking.toVector(observable, timeout, () ⇒ {}, scheduler)

    def toVectorWithAction(timeout: Duration)(actionAfterSubscribe: ⇒ Unit)(implicit scheduler: Scheduler): Vector[T] =
      impl.blocking.toVector(observable, timeout, () ⇒ actionAfterSubscribe, scheduler)

    def dumpOnEach(prefix: ⇒ String)(implicit dumpFunction: DumpFunction = printlnDumpFunction): Observable[T] =
      dumpOnEachMap(prefix, identity)

    def dumpOnEachMap[R](prefix: ⇒ String, mapFunction: T ⇒ R)
                        (implicit dumpFunction: DumpFunction = printlnDumpFunction): Observable[T] =
      impl.debug.dumpOnEachMap(observable, prefix, mapFunction)
  }

  implicit class SchedulerWrapper(scheduler: Scheduler) {
    def advanceTimeBy(duration: FiniteDuration): Unit = {
      scheduler match {
        case testScheduler: TestScheduler ⇒ testScheduler.tick(duration)
        case _ ⇒ Thread.sleep(duration.toMillis)
      }
    }

    def now = scheduler.currentTimeMillis()
  }

}