package info.td.common.monix

/** This is the shared structure that describes the producing and consuming
  * behavior of a particular instance of the `producerConsumer` operator.
  *
  * @tparam T The type of the input (observed, upstream, producer) elements
  * @tparam R The type of the output (generated, downstream, consumer) elements
  */
abstract class ProducerConsumerStrategy[-T, +R] {
  /**
    * The type of the state internal to this particular
    * ProducerConsumerStrategy
    */
  type StateType

  /**
    * This method should generate a new state from an old state and an element
    * that has just been produced.  If this method throws an exception,
    * `producerConsumer` will signal this to the downstream subscriber via
    * `onError`
    *
    * @param state The old state
    * @param element A newly produced element that is to be added to this shared structure
    * @return New state, probably somehow reflecting that a new element has been added.
    */
  def add(state: StateType, element: T): StateType

  /**
    * This method is used by the consumer to retrieve next element from the
    * shared structure.  It should return either an element and a new state
    * (reflecting that the item has just been removed), or None if the old state
    * already represent an empty structure
    *
    * If this method throws an exception, `producerConsumer` will signal this
    * to the downstream subscriber via `onError`
    *
    * @param state The old state
    * @return Some(item) retrieved from the old state if any, along with the new state
    */
  def get(state: StateType): (Option[R], StateType)

  def empty: StateType
}