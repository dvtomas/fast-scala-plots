package info.td.common.monix.impl

import info.td.common.logging.TraceHelpers.DumpFunction
import info.td.common.monix.impl.Internals._
import monifu.concurrent.Cancelable
import monifu.reactive.Ack.Cancel
import monifu.reactive.{Ack, Observable, Observer}

import scala.concurrent.Future
import scala.util.control.NonFatal

private[monix] object debug {
  def dumpOnEachMap[T, R](source: Observable[T], prefix: String, mapFunction: T ⇒ R)(implicit dumpFunction: DumpFunction): Observable[T] =
    Observable.create[T] { subscriber =>
      import subscriber.{scheduler => s}

      source.onSubscribe(new Observer[T] {
        private[this] var pos = 0
        private[this] val downstreamActive = Cancelable {
          pos += 1
          dumpFunction(s"$pos: $prefix canceled")
        }

        def onNext(elem: T): Future[Ack] = {
          // Protects calls to user code from within the operator
          var streamError = true
          try {
            dumpFunction(s"$pos: $prefix-->${mapFunction(elem)}")
            streamError = false

            pos += 1
            subscriber
              .onNext(elem)
              .ifCanceledDoCancel(downstreamActive)(s)
          }
          catch {
            case NonFatal(ex) =>
              if (streamError) {
                subscriber.onError(ex)
                Cancel
              } else
                Future.failed(ex)
          }
        }

        def onError(ex: Throwable) = {
          try {
            dumpFunction(s"$pos: $prefix-->$ex")
            pos += 1
          }
          catch {
            case NonFatal(_) =>
              () // ignore
          }

          subscriber.onError(ex)
        }

        def onComplete() = {
          try {
            dumpFunction(s"$pos: $prefix completed")
            pos += 1
          }
          catch {
            case NonFatal(_) =>
              () // ignore
          }

          subscriber.onComplete()
        }
      })
    }
}
