package info.td.common.monix.impl

import monifu.concurrent.Scheduler
import monifu.reactive.{Ack, Observable}

import scala.collection.immutable.VectorBuilder
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Promise}

private[monix] object blocking {
  def toVector[T](source: Observable[T], timeout: Duration, actionAfterSubscribe: () ⇒ Unit, scheduler: Scheduler): Vector[T] = {

    val vectorBuilder = new VectorBuilder[T]
    val promise = Promise[Vector[T]]()
    source.subscribe(next ⇒ {
      vectorBuilder += next
      Ack.Continue
    },
      error ⇒ promise.failure(error),
      () ⇒ promise.success(vectorBuilder.result)
    )(scheduler)
    actionAfterSubscribe()
    Await.result(promise.future, timeout)
  }
}
