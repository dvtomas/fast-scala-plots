package info.td.common.monix.impl

import java.util.concurrent.Semaphore

import info.td.common.monix.ProducerConsumerStrategy
import monifu.reactive.Ack.{Cancel, Continue}
import monifu.reactive.observers.SynchronousSubscriber
import monifu.reactive.{Ack, Subscriber}

import scala.annotation.tailrec
import scala.util.control.NonFatal
import scala.util.{Failure, Success, Try}

/**
  * A generalized solution to the Consumer-Producer problem.
  *
  * The upstream source is regarded to as a producer generating items. Those
  * are immediately buffered into a shared structure. In parallel, items are
  * being retrieved (consumed) from this shared structure and handed over to a
  * (potentially slow) downstream subscriber (consumer).
  *
  * In the classical producer-consumer the producer generates items, those are
  * buffered, and the very same items in the very same order are being consumed
  * by the consumer.
  *
  * This implementation uses a more general notion of what a "buffer" is. It
  * can be just any shared structure able to produce and consume items. The
  * type and the number of the input and output items doesn't even have to be
  * the same. How the producing/consuming on this structure behaves is
  * specified via an instance of `ProducerConsumerStrategy`, that is handed to
  * the operator at creation time. This allows for easy implementing of the
  * classical producer-consumer problem, but also various MRU caches,
  * aggregation strategies, bufferIntrospective and observeLatestOn operators
  * etc..
  *
  * NOTE: bufferIntrospective has a separate, more efficient implementation. In
  * case you need to only aggregate things that have accumulated during the
  * latest `onNext` of the downstream subscriber, it is better to use that one.
  * *
  * @tparam T the type of the producer (upstream) elements
  * @tparam R the type of the consumer (downstream) elements
  */
private[monix] final class ProducerConsumerSubscriber[-T, +R](underlying: Subscriber[R], strategy: ProducerConsumerStrategy[T, R])
  extends SynchronousSubscriber[T] {
  self ⇒

  implicit val scheduler = underlying.scheduler
  private[this] val batchSizeModulus = scheduler.env.batchSize - 1

  // to be modified only in onError, before upstreamIsComplete
  @volatile private[this] var errorThrown: Throwable = null
  // to be modified only in onError / onComplete
  @volatile private[this] var upstreamIsComplete = false
  // to be modified only by consumer
  @volatile private[this] var downstreamIsDone = false

  private[this] val lock = new Semaphore(1)
  @volatile private[this] var state = strategy.empty
  @volatile private[this] var isPushLoopRunning = false

  def onNext(elem: T): Ack = {
    if (!upstreamIsComplete && !downstreamIsDone) {
      lock.acquire()
      val (maybeError, updatedState) =
        try {
          (None, strategy.add(state, elem))
        } catch {
          case NonFatal(error) ⇒ (Some(error), strategy.empty)
        }
      state = updatedState

      maybeError match {
        case None ⇒ {
          pushToConsumer()
          Continue
        }
        case Some(error) ⇒ {
          if (!upstreamIsComplete && !downstreamIsDone) {
            errorThrown = error
            upstreamIsComplete = true
            pushToConsumer()
          }
          Cancel
        }
      }
    } else {
      Cancel
    }
  }

  def onError(ex: Throwable) = {
    if (!upstreamIsComplete && !downstreamIsDone) {
      errorThrown = ex
      upstreamIsComplete = true
      lock.acquire()
      pushToConsumer()
    }
  }

  def onComplete() = {
    if (!upstreamIsComplete && !downstreamIsDone) {
      upstreamIsComplete = true
      lock.acquire()
      pushToConsumer()
    }
  }

  private[this] def pushToConsumer(): Unit = {
    val shouldRunPushLoop = !isPushLoopRunning
    if (shouldRunPushLoop) {
      isPushLoopRunning = true
    }
    lock.release()
    if (shouldRunPushLoop) {
      scheduler.execute(() ⇒ pushLoop(0))
    }
  }

  private[this] def rescheduled(): Unit = {
    pushLoop(0)
  }

  @tailrec
  private[this] def pushLoop(syncIndex: Int): Unit = {
    if (!downstreamIsDone) {

      lock.acquire()
      val maybeItemToEmit = Try(strategy.get(state)) match {
        case Success((Some(item), newState)) ⇒ {
          state = newState
          Some(item)
        }
        case Success((None, newState)) ⇒ {
          state = newState
          None
        }
        case Failure(error) ⇒ {
          errorThrown = error
          None
        }
      }
      lock.release()
      if (maybeItemToEmit.isEmpty) {
        isPushLoopRunning = false
      }

      val hasError = errorThrown ne null

      maybeItemToEmit match {
        case Some(itemToEmit) ⇒ {
          val ack = underlying onNext itemToEmit
          val nextSyncIndex =
            if (!ack.isCompleted) {
              0
            } else {
              (syncIndex + 1) & batchSizeModulus
            }

          if (nextSyncIndex > 0) {
            if (ack == Continue || ack.value.get == Continue.IsSuccess) {
              // process next
              pushLoop(nextSyncIndex)
            } else if (ack == Cancel || ack.value.get == Cancel.IsSuccess) {
              // ending loop
              downstreamIsDone = true
              isPushLoopRunning = false
            } else if (ack.value.get.isFailure) {
              // ending loop
              downstreamIsDone = true
              isPushLoopRunning = false
              underlying.onError(ack.value.get.failed.get)
            } else {
              // never happens
              downstreamIsDone = true
              isPushLoopRunning = false
              underlying.onError(new MatchError(ack.value.get.toString))
            }
          } else ack.onComplete {
            case Continue.IsSuccess ⇒
              // re-run loop (in different thread)
              rescheduled()

            case Cancel.IsSuccess ⇒
              // ending loop
              downstreamIsDone = true
              isPushLoopRunning = false

            case Failure(ex) ⇒
              // ending loop
              downstreamIsDone = true
              isPushLoopRunning = false
              underlying.onError(ex)

            case other ⇒
              // never happens, but to appease the Scala compiler
              downstreamIsDone = true
              isPushLoopRunning = false
              underlying.onError(new MatchError(s"$other"))
          }
        }
        case None ⇒ {
          if (upstreamIsComplete || hasError) {
            // Race-condition check, remnants of the same implementation in
            // SimpleBufferedSubscriber (Monix 1.0), hopefully it is OK,
            // perhaps redundant
            downstreamIsDone = true
            state = strategy.empty // for GC purposes

            if (errorThrown ne null) {
              underlying.onError(errorThrown)
            } else {
              underlying.onComplete()
            }
          }
        }
      }
    }
  }
}