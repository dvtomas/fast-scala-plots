package info.td.common.monix.impl

import monifu.concurrent.{Cancelable, Scheduler}
import monifu.reactive.Ack
import monifu.reactive.Ack.{Cancel, Continue}

import scala.concurrent.{Future, Promise}
import scala.util.Failure
import scala.util.control.NonFatal

object Internals {

  private[monix] implicit class FutureAckExtensions(val source: Future[Ack]) extends AnyVal {
    /**
      * Helper that triggers [[Cancelable.cancel]] on the given reference,
      * in case our source is a [[Cancel]] or a failure.
      */
    def ifCanceledDoCancel(cancelable: Cancelable)(implicit s: Scheduler): Future[Ack] = {
      if (source.isCompleted) {
        if (source == Continue || source.value.get == Continue.IsSuccess) {
          source // do nothing
        }
        else try {
          cancelable.cancel()
          source
        }
        catch {
          case NonFatal(ex) =>
            Future.failed(ex)
        }
      }
      else {
        val p = Promise[Ack]()
        source.onComplete {
          case Continue.IsSuccess =>
            p.success(Continue)

          case result@(Cancel.IsSuccess | Failure(_)) =>
            try {
              cancelable.cancel()
              p.complete(result)
            }
            catch {
              case NonFatal(ex) =>
                p.failure(ex)
            }

          case other =>
            p.complete(other)
        }

        p.future
      }
    }
  }

}