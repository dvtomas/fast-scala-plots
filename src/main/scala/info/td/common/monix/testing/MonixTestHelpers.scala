package info.td.common.monix.testing

import monifu.concurrent.Scheduler
import monifu.concurrent.schedulers.TestScheduler
import monifu.reactive.Observable

import scala.concurrent.duration.FiniteDuration

object MonixTestHelpers {
  def delaysBetweenEmissions(delays: Iterable[FiniteDuration]): Observable[Long] =
    interval.delaysBetweenEmissions(delays)

  def testWithSchedulers(testMethod: Scheduler ⇒ Unit) = {
    Seq(
      monifu.concurrent.Implicits.globalScheduler,
      TestScheduler()
    ) foreach { scheduler ⇒
      try {
        testMethod(scheduler)
      } catch {
        case e: Throwable ⇒ {
          println(s"Scheduler $scheduler\n: $e") // TODO probably not working!
          throw e
        }
      }
    }
  }
}