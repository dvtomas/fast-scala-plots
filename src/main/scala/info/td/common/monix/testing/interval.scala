package info.td.common.monix.testing

import java.util.concurrent.TimeUnit

import monifu.reactive.Ack.Continue
import monifu.reactive.{Ack, Observable}

import scala.concurrent.duration.FiniteDuration
import scala.util.{Failure, Try}

private[monix] object interval {
  def delaysBetweenEmissions(delays: Iterable[FiniteDuration]): Observable[Long] = {
    val iterator = delays.iterator
    if (!iterator.hasNext) {
      Observable.empty
    } else {
      Observable.create { subscriber ⇒
        import subscriber.{scheduler => s}
        val o = subscriber
        val delay = iterator.next()
          s.scheduleOnce(delay, new Runnable {
          self ⇒
          private[this] var counter = 0L
          private[this] var startedAt = 0L

          def scheduleNext(r: Try[Ack]): Unit = r match {
            case Continue.IsSuccess ⇒
              counter += 1
              if (iterator.hasNext) {
                val delay = {
                  val durationMillis = s.currentTimeMillis() - startedAt
                  val d = iterator.next().toMillis - durationMillis
                  if (d >= 0L) d else 0L
                }
                s.scheduleOnce(delay, TimeUnit.MILLISECONDS, self)
              } else {
                o.onComplete()
              }

            case Failure(ex) =>
              s.reportFailure(ex)
            case _ =>
              () // do nothing else
          }

          def run(): Unit = {
            startedAt = s.currentTimeMillis()
            val ack = o.onNext(counter)

            if (ack.isCompleted)
              scheduleNext(ack.value.get)
            else
              ack.onComplete(scheduleNext)
          }
        })
      }
    }
  }
}