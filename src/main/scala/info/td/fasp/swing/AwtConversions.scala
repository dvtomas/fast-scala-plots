package info.td.fasp.swing

import java.awt.geom.{Line2D, Rectangle2D}
import java.awt.{BasicStroke, Color, Font, Graphics2D}

import info.td.fasp.geom.{ScreenPoint, ScreenRectangle}
import info.td.fasp.paint.{HorizontalAlignment, LineStyle, LineStyleNone, LineStyleSimple, PointStyle, PointStyleDiamond, PointStyleNone, PointStyleRectangle, RichString, VerticalAlignment}

object AwtConversions {

  implicit class ColorExtensions(val color: info.td.fasp.paint.Color) extends AnyVal {
    def toAwt: Color = new Color(color.red, color.green, color.blue)
  }

  implicit class FontExtensions(val font: info.td.fasp.paint.FontProperties) extends AnyVal {
    def toAwt = {
      val style = font.style match {
        case info.td.fasp.paint.Font.Plain ⇒ Font.PLAIN
        case info.td.fasp.paint.Font.Bold ⇒ Font.BOLD
        case info.td.fasp.paint.Font.Italic ⇒ Font.ITALIC
        case info.td.fasp.paint.Font.BoldItalic ⇒ Font.BOLD | Font.ITALIC
      }
      new Font(font.name, style, font.size.toInt)
    }
  }

  private[this] object PointStyleExtension {
    private val xShapeDiamond = Array[Int](0, -2, 0, 2, 0)
    private val yShapeDiamond = Array[Int](-2, 0, 2, 0, -2)
  }

  implicit class PointStyleExtension(val pointStyle: PointStyle) extends AnyVal {
    def draw(g: Graphics2D, x: Double, y: Double): Unit = pointStyle match {
      case PointStyleNone ⇒
      case PointStyleRectangle(color) ⇒ {
        g.setColor(color.toAwt)
        g.setStroke(new BasicStroke(1))
        g.draw(new Rectangle2D.Double(x - 1.0, y - 1.0, 2.0, 2.0))
      }
      case PointStyleDiamond(color, width) ⇒ {
        g.setStroke(new BasicStroke(width.toInt))
        g.setColor(color.toAwt)
        def xOffset = x
        def yOffset = y
        val xShape = PointStyleExtension.xShapeDiamond
        val yShape = PointStyleExtension.yShapeDiamond
        for (xy: Array[(Double, Double)] ← (xShape.map(_ + xOffset) zip yShape.map(_ + yOffset)).sliding(2)) {
          g.draw(new Line2D.Double(xy(0)._1, xy(0)._2, xy(1)._1, xy(1)._2))
        }
      }
    }

    def draw(g: Graphics2D, point: ScreenPoint): Unit = draw(g, point.x, point.y)
  }

  implicit class LineStyleExtension(val lineStyle: LineStyle) extends AnyVal {
    def draw(g: Graphics2D, x1: Double, y1: Double, x2: Double, y2: Double) = lineStyle match {
      case LineStyleNone ⇒
      case LineStyleSimple(color, width) ⇒ {
        g.setColor(color.toAwt)
        g.setStroke(new BasicStroke(width.toFloat))
        g.draw(new Line2D.Double(x1, y1, x2, y2))
      }
    }
  }

  implicit class ScreenRectangleExtensions(val screenRectangle: ScreenRectangle) extends AnyVal {
    def fill(g: Graphics2D, color: info.td.fasp.paint.Color): Unit = {
      g.setBackground(color.toAwt)
      g.fill(new Rectangle2D.Double(screenRectangle.left, screenRectangle.top, screenRectangle.width, screenRectangle.height))
    }

    def stroke(g: Graphics2D, color: info.td.fasp.paint.Color, width: Double): Unit = {
      g.setBackground(color.toAwt)
      g.setStroke(new BasicStroke(width.toInt))
      g.draw(new Rectangle2D.Double(screenRectangle.left, screenRectangle.top, screenRectangle.width, screenRectangle.height))
    }
  }

  implicit class RichStringExtensions(val richString: RichString) extends AnyVal {
    private[this] def string: String = richString.string

    private[this] def topAligned(y: Double) = y + richString.height

    private[this] def bottomAligned(y: Double) = y - 3

    private[this] def vCenterAligned(y: Double) = y + (richString.height / 2)

    def setFontAndGetWidth(g: Graphics2D) = {
      g.setFont(richString.properties.toAwt)
      g.getFontMetrics.getStringBounds(string, g).getWidth.toInt
    }

    private def setFontAndLeftAligned(g: Graphics2D, x: Double) = {
      g.setFont(richString.properties.toAwt)
      x
    }

    private def setFontAndHCenterAligned(g: Graphics2D, x: Double) = x - (setFontAndGetWidth(g) / 2)

    private def setFontAndRightAligned(g: Graphics2D, x: Double) = x - setFontAndGetWidth(g)

    protected def _drawAt(g: Graphics2D, x: Double, y: Double) {
      g.setColor(richString.properties.color.toAwt)
      g.drawString(string, x.toInt, y.toInt)
    }

    def drawAt(g: Graphics2D, x: Double, y: Double, horizontalAlignment: HorizontalAlignment, verticalAlignment: VerticalAlignment): Unit = {
      if (!string.isEmpty) {
        val alignedX = horizontalAlignment match {
          case HorizontalAlignment.left ⇒ setFontAndLeftAligned(g, x)
          case HorizontalAlignment.center ⇒ setFontAndHCenterAligned(g, x)
          case HorizontalAlignment.right ⇒ setFontAndRightAligned(g, x)
        }
        verticalAlignment match {
          case VerticalAlignment.top ⇒ _drawAt(g, alignedX, topAligned(y))
          case VerticalAlignment.center ⇒ _drawAt(g, alignedX, vCenterAligned(y))
          case VerticalAlignment.bottom ⇒ _drawAt(g, alignedX, bottomAligned(y))
        }
      }
    }

    def drawAt(g: Graphics2D, position: ScreenPoint, horizontalAlignment: HorizontalAlignment, verticalAlignment: VerticalAlignment): Unit = {
      drawAt(g, position.x, position.y, horizontalAlignment, verticalAlignment)
    }

    /** Renders the string roughly at position aligned in such a way, that it does not overlap the position, but is in the quadrant close to a certain lean point. */
    def drawAtAndLeaningTo(g: Graphics2D, position: ScreenPoint, leanTo: ScreenPoint): Unit = {
      val (horizontalAlignment, xOffset) = if (position.x < leanTo.x) (HorizontalAlignment.left, 5) else (HorizontalAlignment.right, -5)
      val (verticalAlignment, yOffset) = if (position.y < leanTo.y) (VerticalAlignment.top, 5) else (VerticalAlignment.bottom, -5)
      drawAt(g, position.x + xOffset, position.y + yOffset, horizontalAlignment, verticalAlignment)
    }
  }

}