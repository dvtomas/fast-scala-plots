package info.td.fasp.swing.plot

import java.awt.Graphics2D

import info.td.fasp.figurepart.Axes
import info.td.fasp.plot.XYPlot

object XYPlotRenderer {
  def render(g: Graphics2D, plot: XYPlot, axes: Axes) = {
    ContinuousLinePlotter.plot(g, plot.data, axes, plot.linePlottingProperties)
  }
}