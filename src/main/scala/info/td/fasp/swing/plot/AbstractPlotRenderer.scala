package info.td.fasp.swing.plot

import java.awt.Graphics2D

import info.td.fasp.figurepart.Axes
import info.td.fasp.plot.Plot

abstract class AbstractPlotRenderer {
  def render(g: Graphics2D, plot: Plot, axes: Axes): Unit
}