package info.td.fasp.swing.plot

import java.awt.Graphics2D
import java.util.Date

import info.td.fasp.figurepart.Axes
import info.td.fasp.geom.ScreenPoint
import info.td.fasp.paint.LinePlottingProperties
import info.td.fasp.plot.XYPlotData
import info.td.fasp.swing.AwtConversions._

object ContinuousLinePlotter {
  var lastTime = new Date()

  def plot(
            g: Graphics2D,
            data: XYPlotData,
            axes: Axes,
            linePlottingProperties: LinePlottingProperties
          ) {
    var maybeLastPoint: Option[ScreenPoint] = None
    var wasPointBeforeLastPoint: Boolean = false

    (0 until data.numberOfPoints) foreach { index ⇒
      val newWasPointBeforeLastPoint = maybeLastPoint.nonEmpty
      val x = data.unsafeXValue(index)
      val y = data.unsafeYValue(index)

      if (!x.isNaN && !y.isNaN) {
        val screenX = axes.axisX.screenValue(x)
        val screenY = axes.axisY.screenValue(y)

        maybeLastPoint foreach (lastPoint ⇒ linePlottingProperties.lineStyle.draw(g, lastPoint.x, lastPoint.y, screenX, screenY))
        linePlottingProperties.pointStyle.draw(g, screenX, screenY)
        maybeLastPoint = Some(ScreenPoint(screenX, screenY))
      } else {
        if (!wasPointBeforeLastPoint) {
          // last point was in the middle between two non-points. We need to draw at least a pixel at its position.
          maybeLastPoint.foreach(lastPoint ⇒ linePlottingProperties.pointStyle.draw(g, lastPoint))
        }
        maybeLastPoint = None
      }

      wasPointBeforeLastPoint = newWasPointBeforeLastPoint
    }

  }
}