package info.td.fasp.swing.utils


import java.awt._
import java.awt.image.BufferedImage
import javax.swing.{ImageIcon, JFrame, JLabel}

import info.td.fasp.geom.ScreenRectangle

object Graphics2DUtils {

  private case class Hint(key: RenderingHints.Key, on: Object, off: Object)

  private val hints = Seq(
    Hint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF),
    Hint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY, RenderingHints.VALUE_RENDER_SPEED),
    Hint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON, RenderingHints.VALUE_ANTIALIAS_OFF),
    Hint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE, RenderingHints.VALUE_STROKE_DEFAULT)
  )

  def setAntiAliasedRendering(g: Graphics2D, enabled: Boolean) {
    for (hint ← hints) {
      g.setRenderingHint(hint.key, if (enabled) hint.on else hint.off)
    }
  }

  def setAntiAliasedRendering(targetG: Graphics2D, sourceG: Graphics2D) {
    for (hint ← hints) {
      targetG.setRenderingHint(hint.key, sourceG.getRenderingHint(hint.key))
    }
  }

  def withClipDo[T](g: Graphics2D, bounds: ScreenRectangle)(action: ⇒ T) = {
    val originalClip = g.getClipBounds
    g.setClip(bounds.left.toInt, bounds.top.toInt, bounds.width.toInt, bounds.height.toInt)
    try {
      action
    } finally {
      g.setClip(originalClip)
    }
  }

  def setColorAndAlpha(g: Graphics2D, color: Color, alpha: Float) {
    val composite = if (alpha == 1.0f) {
      AlphaComposite.Src
    } else {
      AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha)
    }
    g.setComposite(composite)
    g.setColor(color)
  }

  def readableForegroundColorForBackground(color: Color, grayMultiplier: Float = 1.0f): Color = {
    val gray = (color.getRed * 0.299) + (color.getGreen * 0.587) + (color.getBlue * 0.114)
    val treshold = 186
    if (gray * grayMultiplier < treshold)
      Color.WHITE
    else
      Color.BLACK
  }

  def displayImage(image: BufferedImage) {
    val frame = new JFrame()
    frame.getContentPane.setLayout(new FlowLayout())
    frame.getContentPane.add(new JLabel(new ImageIcon(image)))
    frame.pack()
    frame.setVisible(true)
  }

  def createCompatibleImage(width: Int, height: Int): BufferedImage = {
    val graphicsConfiguration = GraphicsEnvironment.getLocalGraphicsEnvironment.getDefaultScreenDevice.getDefaultConfiguration
    graphicsConfiguration.createCompatibleImage(width, height)
  }
}