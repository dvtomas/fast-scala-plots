package info.td.fasp.axispainter

import info.td.fasp.axis.Axis
import info.td.fasp.paint.Color
import info.td.fasp.utils.MathUtils

case class Tick(screenValue: Double, dataValue: Double)

case class TicksPlacement(ticks: Seq[Tick], positionOfDigitThatChanges: Int)

case class TicksSettings(minimalTicksDistance: Double, tickLength: Double, color: Color) {
  private[this] def reasonableTickDistances = 1.0 :: 2.0 :: 5.0 :: 10.0 :: 20.0 :: 50.0 :: 100.0 :: 200.0 :: 500.0 :: 1000.0 :: Nil

  def computeTicksPlacement(axis: Axis): TicksPlacement = {
    val screenRange = axis.screenRange
    val dataRange = axis.dataRange

    if (Math.abs(screenRange.length) <= 1 || dataRange.length == 0.0)
      TicksPlacement(Seq.empty, 0)
    else {
      val k = dataRange.length / screenRange.length
      val decadicKExponent = MathUtils.decadicExponent(Math.abs(k))
      val decadicKBase = k / Math.pow(10.0, decadicKExponent)
      val absMinimalTicksDistance = Math.abs(decadicKBase * minimalTicksDistance)
      val absTicksDistance = reasonableTickDistances
        .find(_ > absMinimalTicksDistance)
        .getOrElse(reasonableTickDistances.last)
      val ticksDistance = if (k < 0) -absTicksDistance else absTicksDistance
      val step = ticksDistance.toDouble * Math.pow(10.0, decadicKExponent)

      val (start, end) = if (k < 0 ^ dataRange.minimum < dataRange.maximum)
        (dataRange.minimum, dataRange.maximum)
      else
        (dataRange.maximum, dataRange.minimum)

      val wholeStepStart = ((start + step / 2) / Math.abs(step)).round * Math.abs(step)
      val absStepTreshold = Math.abs(step) / 10.0 // Used to get pure zero in case of rounding errors
      val takeNextCondition: Tick ⇒ Boolean = if (step > 0) _.dataValue <= end else _.dataValue >= end
      val ticks = Stream.iterate(0)(_ + 1)
        .map { index ⇒
          val dataValue = wholeStepStart + (index.toDouble * step)
          val fixedDataValue = if (Math.abs(dataValue) < absStepTreshold) 0.0 else dataValue
          Tick(axis.screenValue(fixedDataValue), dataValue)
        }
        .takeWhile(takeNextCondition)
        .toVector

      TicksPlacement(
        ticks,
        if (wholeStepStart.abs == 0.0) {
          0
        } else {
          MathUtils.decadicExponent(wholeStepStart) - MathUtils.decadicExponent(Math.abs(step)) + 1
        }
      )
    }
  }
}

class AxisPainter

abstract class AxisPainterX extends AxisPainter

abstract class AxisPainterY extends AxisPainter
