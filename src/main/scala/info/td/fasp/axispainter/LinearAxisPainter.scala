package info.td.fasp.axispainter

import info.td.fasp.paint.{Color, FontProperties}
import info.td.fasp.utils.{ValueFormatter, ValueFormatterLocale}

trait LinearAxisPainter extends AxisPainter

case class AxisPaintingProperties(
                                   minorTicksSettings: TicksSettings,
                                   majorTicksSettings: TicksSettings,
                                   fontProperties: FontProperties
                                 ) {
  def valueString(value: Double, digits: Int)(implicit valueFormatterLocale: ValueFormatterLocale): String =
    ValueFormatter.valueString(value, digits)
}

case class LinearAxisPainterX(
                               properties: AxisPaintingProperties = AxisPaintingProperties(
                                 TicksSettings(5, 2, Color.lightGray),
                                 TicksSettings(80, 3, Color.black),
                                 FontProperties()
                               )) extends AxisPainterX with LinearAxisPainter

case class LinearAxisPainterY(
                               properties: AxisPaintingProperties = AxisPaintingProperties(
                                 TicksSettings(5, 2, Color.lightGray),
                                 TicksSettings(30, 3, Color.black),
                                 FontProperties()
                               )
                             ) extends AxisPainterY with LinearAxisPainter

