package info.td.fasp.demo.javafx

import java.util.Locale
import javafx.application.{Application, Platform}
import javafx.fxml.FXMLLoader
import javafx.scene.layout.StackPane
import javafx.scene.{Node, Parent, Scene}
import javafx.stage.{Stage, WindowEvent}

import com.typesafe.scalalogging.LazyLogging
import info.td.fasp.axis._
import info.td.fasp.axispainter.{LinearAxisPainterX, LinearAxisPainterY}
import info.td.fasp.controller.PlotController.SetAxesDataRanges
import info.td.fasp.controller._
import info.td.fasp.figure.{Figure, FigureSource}
import info.td.fasp.figurepart._
import info.td.fasp.geom.DataRange
import info.td.fasp.javafx.controller.ControllerOverlaySource
import info.td.fasp.javafx.pick.PickOverlaySource
import info.td.fasp.javafx.{JavaFXFigurePane, RenderActionFactory}
import info.td.fasp.paint._
import info.td.fasp.pick.{FigurePartPicker, PickResult, PickStrategy, PickerLinkStrategy}
import info.td.fasp.plot.{LinePlotPickDescriber, PlotKey, XYPlot, XYPlotData}
import info.td.fasp.utils.ValueFormatterLocale
import monifu.concurrent.Scheduler
import monifu.reactive.Observable

import scala.concurrent.duration._
import scala.reflect.ClassTag

class JavaFXDemo extends Application with LazyLogging {

  private[this] implicit val valueFormatterLocale = ValueFormatterLocale(Locale.US)

  private[this] class DemoXYPlotData(_numberOfPoints: Int, yValueFunc: Int ⇒ Double) extends XYPlotData {
    def numberOfPoints = _numberOfPoints

    def unsafeXValue(index: Int) = index.toDouble

    def unsafeYValue(index: Int) = yValueFunc(index)
  }

  private[this] def combineOverlaySources(sources: Observable[Seq[Node]]*): Observable[Seq[Node]] =
    Observable.combineLatestList[Seq[Node]](sources.map(source ⇒ Observable(Seq.empty) ++ source): _*).map(_.flatten)

  private[this] def controllerOverlaySource(controllerOutputSource: Observable[Option[PlotController.State]]): Observable[Seq[Node]] = {
    ControllerOverlaySource.nodesFromControllerOutput(controllerOutputSource)
  }

  private[this] def pickOverlaySource(
                                       figureController: FigureController,
                                       figureSource: Observable[Figure],
                                       pickStrategy: PickStrategy,
                                       pickerLinkStrategy: PickerLinkStrategy
                                     )(implicit scheduler: Scheduler): Observable[Seq[Node]] = {
    val picker = FigurePartPicker(pickStrategy)
    val pickResultsSource: Observable[Seq[PickResult]] =
      PickResult.pickResultSource(
        FigurePartPicker(pickStrategy),
        figureController.maybePickRequestSource,
        figureSource,
        pickerLinkStrategy
      )

    val pickOverlaySource = PickOverlaySource.forPicker(picker).nodesFromPickResults(figureSource, pickResultsSource)
    pickOverlaySource
  }

  private[this] def overlaySource(
                                   figureController: FigureController,
                                   figureSource: Observable[Figure],
                                   pickStrategy: PickStrategy,
                                   pickerLinkStrategy: PickerLinkStrategy
                                 )(implicit scheduler: Scheduler): Observable[Seq[Node]] = {
    combineOverlaySources(
      pickOverlaySource(figureController, figureSource, pickStrategy, pickerLinkStrategy),
      controllerOverlaySource(figureController.outputs.maybePlotControllerStateSource)
    )
  }

  private[this] abstract class DemoTab {
    def figurePane: JavaFXFigurePane

    def subscribe()(implicit scheduler: Scheduler): Unit
  }

  private[this] object BasicPlotDemoTab extends DemoTab {
    val axesOnScreenSource = new AxesOnScreenSource(
      Seq(
        new AxesOnScreenSpec(
          FigurePartKey('basicFigure),
          identity, _.expanded(-50, -30, -20, -30),
          LinearAxisXFactory, DataRange(0, 100),
          LinearAxisYFactory, DataRange(-100, 100)
        )
      ), SetAxesDataRangesStrategy.Independent)

    val propertiesSource = Observable.unit(Map(
      FigurePartKey('basicFigure) → FigurePartProperties(
        new RichString("Plot title", FontProperties(size = 16, color = Color.blue)),
        new RichString("Label X"),
        new RichString("Label y"),
        LinearAxisPainterX(),
        LinearAxisPainterY()
      )
    ))

    def plotData(plotIndex: Int) = new DemoXYPlotData(2000, index ⇒ Math.sin((index.toDouble + plotIndex) / 20.0) * 100.0)

    val plotUpdatesSource =
      Observable.interval(100.milli).map(index ⇒ Map(
        FigurePartKey('basicFigure) → Seq(XYPlot(PlotKey('basicPlot), "Plot A", plotData(index.toInt)))
      ))

    val figurePane = new JavaFXFigurePane(new RenderActionFactory())

    val figureController = new FigureController()

    val figureSource = new FigureSource

    override def subscribe()(implicit scheduler: Scheduler): Unit = {
      val controllerEventsSource = ControllerEventSourceDesktop.controllerEventsSource(figurePane.eventsSource())
      axesOnScreenSource.subscribe(figurePane.resizeEventsSource(), figureController.setAxesDataRangesSource)
      figureController.subscribe(controllerEventsSource, axesOnScreenSource.axesOnScreenSource)
      figureSource.subscribe(axesOnScreenSource.axesOnScreenSource, propertiesSource, plotUpdatesSource)
      figurePane.subscribe(
        figureSource.changedFigurePartSource,
        overlaySource(figureController, figureSource.figureSource, PickStrategy.XY, PickerLinkStrategy.None)
      )
    }
  }

  private[this] object ConnectedPlotsDemoTab extends DemoTab {
    val pointsPerPlot = 30000

    // An example of custom axes linking strategy. All the plots have linked X axis, and furthermore plots 2 and 3 also have linked the Y axis
    object DemoLinkStrategy extends SetAxesDataRangesStrategy {
      override def applySetAxesDataRanges(
                                           setAxesDataRanges: SetAxesDataRanges,
                                           axesOnScreenCollection: Seq[AxesOnScreen]
                                         ): Seq[AxesOnScreen] = {
        axesOnScreenCollection.map(axesOnScreen ⇒
          if (axesOnScreen.key == setAxesDataRanges.key || (setAxesDataRanges.key.key != 'part1 && axesOnScreen.key.key != 'part1)) {
            axesOnScreen.copy(axes = axesOnScreen.axes.withNewDataRanges(setAxesDataRanges.dataRangeX, setAxesDataRanges.dataRangeY))
          } else {
            axesOnScreen.copy(axes = axesOnScreen.axes.copy(axisX = axesOnScreen.axes.axisX.withNewDataRange(setAxesDataRanges.dataRangeX)))
          }
        )
      }
    }

    val axesOnScreenSource = new AxesOnScreenSource(
      Seq(
        new AxesOnScreenSpec(
          FigurePartKey('part1),
          _.verticalSplit(0, 3, 1), _.expanded(-50, -30, -20, -30),
          LinearAxisXFactory, DataRange(0, 100),
          LinearAxisYFactory, DataRange(-100, 100)
        ),
        new AxesOnScreenSpec(
          FigurePartKey('part2),
          _.verticalSplit(1, 3, 1), _.expanded(-50, -30, -20, -30),
          LinearAxisXFactory, DataRange(0, 100),
          LinearAxisYFactory, DataRange(-100, 100)
        ),
        new AxesOnScreenSpec(
          FigurePartKey('part3),
          _.verticalSplit(2, 3, 1), _.expanded(-50, -30, -20, -30),
          LinearAxisXFactory, DataRange(0, 100),
          LinearAxisYFactory, DataRange(-100, 100)
        )
      ), DemoLinkStrategy)

    val propertiesSource = Observable.unit(Map(
      FigurePartKey('part1) → FigurePartProperties(
        new RichString("Plot Foo"),
        new RichString("Footages"),
        new RichString("Feet"),
        LinearAxisPainterX(),
        LinearAxisPainterY()
      ),
      FigurePartKey('part2) → FigurePartProperties(
        new RichString("Plot Bar"),
        new RichString("Bars"),
        new RichString("Barity"),
        LinearAxisPainterX(),
        LinearAxisPainterY()
      ),
      FigurePartKey('part3) → FigurePartProperties(
        new RichString("Plot Baz"),
        new RichString("Bazookas"),
        new RichString("Bazity"),
        LinearAxisPainterX(),
        LinearAxisPainterY()
      )
    ))

    val plotUpdatesSource =
      Observable.unit(Map(
        FigurePartKey('part1) → Seq(XYPlot(
          PlotKey('plot1a), "Plot 1A",
          new DemoXYPlotData(pointsPerPlot * 3 / 2, index ⇒ Math.sin(index.toDouble / 20.0) * 100.0),
          LinePlottingProperties(),
          LinePlotPickDescriber.Verbose(3, 3)
        )),
        FigurePartKey('part2) → Seq(XYPlot(
          PlotKey('plot2a), "Plot 2A",
          new DemoXYPlotData(pointsPerPlot * 3 / 2, index ⇒ Math.sin((index.toDouble + 0.3) / 100.0) * 70.0),
          LinePlottingProperties(LineStyleSimple(Color.orange)),
          LinePlotPickDescriber.Verbose(3, 3)
        )),
        FigurePartKey('part3) → Seq(
          XYPlot(
            PlotKey('plot3a), "Plot 3A",
            new DemoXYPlotData(pointsPerPlot, index ⇒ Math.sin((index.toDouble + 0.4) / 50.0) * 60.0),
            LinePlottingProperties(LineStyleSimple(Color.gray)),
            LinePlotPickDescriber.Verbose(3, 3)
          ),
          XYPlot(
            PlotKey('plot3b), "Plot 3B",
            new DemoXYPlotData(pointsPerPlot, index ⇒ Math.sin((index.toDouble + 2) / 10.0) * 80.0),
            LinePlottingProperties(LineStyleSimple(Color.green, 3), PointStyleNone),
            LinePlotPickDescriber.Verbose(3, 3)
          )
        )
      ))

    val figurePane = new JavaFXFigurePane(new RenderActionFactory())

    val figureController = new FigureController()

    val figureSource = new FigureSource

    override def subscribe()(implicit scheduler: Scheduler): Unit = {
      val controllerEventsSource = ControllerEventSourceDesktop.controllerEventsSource(figurePane.eventsSource())
      axesOnScreenSource.subscribe(figurePane.resizeEventsSource(), figureController.setAxesDataRangesSource)
      figureController.subscribe(controllerEventsSource, axesOnScreenSource.axesOnScreenSource)
      figureSource.subscribe(axesOnScreenSource.axesOnScreenSource, propertiesSource, plotUpdatesSource)
      figurePane.subscribe(
        figureSource.changedFigurePartSource,
        overlaySource(figureController, figureSource.figureSource, PickStrategy.Vertical, PickerLinkStrategy.SameDataPoints)
      )
    }
  }

  def start(primaryStage: Stage) {

    val fxmlLoader = new FXMLLoader()
    fxmlLoader.setLocation(getClass.getResource("/JavaFXDemo.fxml"))
    val root = fxmlLoader.load().asInstanceOf[Parent]

    implicit val scheduler = monifu.concurrent.Implicits.globalScheduler

    Map(
      "#basicPlotHolder" → BasicPlotDemoTab,
      "#connectedPlotsHolder" → ConnectedPlotsDemoTab
    ) foreach { case (selector, demoTab) ⇒
      withLookedUpNodeDo[StackPane](root, selector)(_.getChildren.add(demoTab.figurePane.pane))
      scheduler.execute {
        demoTab.subscribe()
      }
    }

    primaryStage.setTitle("JavaFX Fast scala plots demonstration")
    primaryStage.setScene(new Scene(root, 800, 600))
    primaryStage.setOnCloseRequest((t: WindowEvent) ⇒ {
      Platform.exit()
      System.exit(0)
    })
    primaryStage.show()
  }

  private[this] def withLookedUpNodeDo[NodeClass](parent: Parent, selector: String)(f: NodeClass ⇒ Unit)(implicit tag: ClassTag[NodeClass]) = {
    parent.lookup(selector) match {
      case node: NodeClass ⇒ f(node)
      case other ⇒ logger.error(s"Could not find node of class ${tag.runtimeClass.getCanonicalName} with selector $selector, found this instead: $other")
    }
  }
}