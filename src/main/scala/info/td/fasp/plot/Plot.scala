package info.td.fasp.plot

case class PlotKey(key: Symbol)

trait Plot {
  def plotKey: PlotKey

  def name: String
}