package info.td.fasp.plot

import info.td.fasp.geom.DataPoint
import info.td.fasp.paint.LinePlottingProperties

abstract class XYPlotData extends PointData {
  def numberOfPoints: Int

  def unsafeXValue(index: Int): Double

  def unsafeYValue(index: Int): Double

  final def iterateValues: Iterator[DataPoint] = new Iterator[DataPoint] {
    private var index: Int = 0

    def hasNext = index < numberOfPoints

    def next() = {
      val oldIndex = index
      index += 1
      unsafePoint(oldIndex)
    }
  }

  final def unsafePoint(index: Int): DataPoint = DataPoint(unsafeXValue(index), unsafeYValue(index))
}

case class XYPlot(
                   plotKey: PlotKey,
                   name: String,
                   data: XYPlotData,
                   linePlottingProperties: LinePlottingProperties = LinePlottingProperties(),
                   pickDescriber: LinePlotPickDescriber = LinePlotPickDescriber.Verbose(3, 3)
                 ) extends LinePlot