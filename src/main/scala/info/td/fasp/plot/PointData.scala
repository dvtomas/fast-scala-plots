package info.td.fasp.plot

import info.td.fasp.geom.DataPoint

abstract class PointData {
  def numberOfPoints: Int

  def unsafePoint(index: Int): DataPoint

  final def maybePoint(index: Int): Option[DataPoint] =
    if (index >= 0 && index < numberOfPoints)
      Some(unsafePoint(index))
    else
      None
}