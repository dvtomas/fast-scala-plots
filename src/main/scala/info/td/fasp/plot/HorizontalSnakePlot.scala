package info.td.fasp.plot

import info.td.fasp.geom.EquidistantRange
import info.td.fasp.paint.LinePlottingProperties

abstract class SnakePlotData extends PointData {
  def range: EquidistantRange

  def value(index: Int): Double
}

case class HorizontalSnakePlot(
                                plotKey: PlotKey,
                                name: String,
                                data: SnakePlotData,
                                linePlottingProperties: LinePlottingProperties,
                                pickDescriber: LinePlotPickDescriber
                              ) extends LinePlot
