package info.td.fasp.plot

import info.td.fasp.paint.LinePlottingProperties

trait LinePlot extends Plot {
  def data: PointData

  def linePlottingProperties: LinePlottingProperties

  def pickDescriber: LinePlotPickDescriber
}