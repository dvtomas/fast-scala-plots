package info.td.fasp.plot

import info.td.fasp.geom.DataPoint
import info.td.fasp.utils.{MathUtils, ValueFormatterLocale}

abstract class LinePlotPickDescriber {
  def describe(plot: LinePlot, pointIndex: Int)(implicit valueFormatterLocale: ValueFormatterLocale): String
}

object LinePlotPickDescriber {
  def valueString(value: Double, digits: Int)(implicit valueFormatterLocale: ValueFormatterLocale): String = {
    val lowExponentForScientificNotation = -3
    val highExponentForScientificNotation = 5

    if (value == 0.0) {
      "0"
    } else {
      val decadicExponent: Int = MathUtils.decadicExponent(value)

      val format = if (decadicExponent < lowExponentForScientificNotation) {
        s"%.${digits - 1}E"
      } else if (decadicExponent < highExponentForScientificNotation) {
        s"%.${(digits - decadicExponent - 1) max 0}f"
      } else {
        s"%.${digits - 1}E"
      }
      val result = format.formatLocal(valueFormatterLocale.locale, value)
      result
    }
  }

  private[this] def describeMaybePoint(plot: LinePlot, pointIndex: Int)(f: DataPoint ⇒ String) =
    plot.data.maybePoint(pointIndex) map f getOrElse ""

  case class Verbose(xDigits: Int, yDigits: Int) extends LinePlotPickDescriber {
    override def describe(plot: LinePlot, pointIndex: Int)(implicit valueFormatterLocale: ValueFormatterLocale): String =
      describeMaybePoint(plot, pointIndex) { dataPoint ⇒
        val xValue = valueString(dataPoint.x, xDigits)
        val yValue = valueString(dataPoint.y, yDigits)
        s"${plot.name}($xValue)=$yValue"
      }
  }

  case class YValue(digits: Int) extends LinePlotPickDescriber {
    override def describe(plot: LinePlot, pointIndex: Int)(implicit valueFormatterLocale: ValueFormatterLocale): String =
      describeMaybePoint(plot, pointIndex) { dataPoint ⇒
        s"${valueString(dataPoint.y, digits)}"
      }
  }

  case class XYValues(xDigits: Int, yDigits: Int) extends LinePlotPickDescriber {
    override def describe(plot: LinePlot, pointIndex: Int)(implicit valueFormatterLocale: ValueFormatterLocale): String =
      describeMaybePoint(plot, pointIndex) { dataPoint ⇒
        val xValue = valueString(dataPoint.x, xDigits)
        val yValue = valueString(dataPoint.y, yDigits)
        s"$xValue: $yValue"
      }
  }

}

