package info.td.fasp.figurepart

import info.td.fasp.axis.{AxisX, AxisY, LinearAxisX, LinearAxisY}
import info.td.fasp.axispainter.{AxisPainterX, AxisPainterY, NullAxisPainterX, NullAxisPainterY}
import info.td.fasp.controller.AxesOnScreen
import info.td.fasp.geom._
import info.td.fasp.paint.{Color, RichString}
import info.td.fasp.plot.Plot

case class FigurePartKey(key: Symbol)

case class Axes(axisX: AxisX, axisY: AxisY) {
  def screenBounds: ScreenRectangle = ScreenRectangle(
    axisX.screenRange.minimum, axisY.screenRange.minimum,
    axisX.screenRange.maximum, axisY.screenRange.maximum
  )

  def withNewScreenBounds(newScreenBounds: ScreenRectangle): Axes =
    copy(
      axisX = axisX.withNewScreenRange(newScreenBounds.horizontalRange),
      axisY = axisY.withNewScreenRange(newScreenBounds.verticalRange)
    )

  def withNewDataRanges(newDataRangeX: DataRange, newDataRangeY: DataRange) =
    copy(
      axisX = axisX.withNewDataRange(newDataRangeX),
      axisY = axisY.withNewDataRange(newDataRangeY)
    )

  def screenPoint(dataPoint: DataPoint): ScreenPoint =
    ScreenPoint(axisX.screenValue(dataPoint.x), axisY.screenValue(dataPoint.y))

  def dataPoint(screenPoint: ScreenPoint): DataPoint =
    DataPoint(axisX.dataValue(screenPoint.x), axisY.dataValue(screenPoint.y))
}

object Axes {
  val empty = Axes(LinearAxisX(ScreenRange(0, 0), DataRange(0, 0)), LinearAxisY(ScreenRange(0, 0), DataRange(0, 0)))
}

case class FigurePartProperties(
                                 title: RichString,
                                 xLabel: RichString,
                                 yLabel: RichString,
                                 axisPainterX: AxisPainterX,
                                 axisPainterY: AxisPainterY,
                                 backgroundColor: Color = Color.white,
                                 borderColor: Color = Color.darkGray,
                                 axesBorderColor: Color = Color.black,
                                 axesBackgroundColor: Color = Color.white,
                                 antiAliasedRendering: Boolean = true
                               )

object FigurePartProperties {
  def empty = FigurePartProperties(
    new RichString(""), new RichString(""), new RichString(""),
    NullAxisPainterX, NullAxisPainterY
  )
}

case class FigurePart(
                       key: FigurePartKey,
                       screenBounds: ScreenRectangle,
                       axes: Axes,
                       plots: Seq[Plot],
                       properties: FigurePartProperties
                     ) {
  final def axesScreenBounds = axes.screenBounds

  final def axesOnScreen = AxesOnScreen(key, screenBounds, axes)

  final def withNewAxesOnScreen(newAxesOnScreen: AxesOnScreen): FigurePart =
    copy(
      screenBounds = newAxesOnScreen.figurePartBounds,
      axes = newAxesOnScreen.axes
    )

  final def withUpdatedPlot(newPlot: Plot): FigurePart = {
    val updatedPlots =
      if (plots.exists(_.plotKey == newPlot.plotKey)) {
        plots.map(oldPlot ⇒ if (oldPlot.plotKey == newPlot.plotKey) newPlot else oldPlot)
      } else {
        plots :+ newPlot
      }
    copy(plots = updatedPlots)
  }

  final def withUpdatedPlots(newPlots: Seq[Plot]): FigurePart = {
    newPlots.foldLeft(this)(_ withUpdatedPlot _)
  }
}
