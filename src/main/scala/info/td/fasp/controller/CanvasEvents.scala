package info.td.fasp.controller

import info.td.fasp.geom.{ScreenPoint, ScreenRectangle}

object MouseButton {

  abstract sealed class MouseButton

  case object Primary extends MouseButton

  case object Middle extends MouseButton

  case object Secondary extends MouseButton

}

abstract sealed class FigureEvent

abstract class Key

object Key {

  case class Modifiers(isShiftDown: Boolean, isControlDown: Boolean, isAltDown: Boolean)

  case object Escape extends Key

}

object FigureEvent {

  case class Resize(screenRectangle: ScreenRectangle) extends FigureEvent

  case class KeyPressed(key: Key) extends FigureEvent

  case class Click(button: MouseButton.MouseButton, screenPoint: ScreenPoint, modifiers: Key.Modifiers) extends FigureEvent

  case class Release(button: MouseButton.MouseButton, screenPoint: ScreenPoint, modifiers: Key.Modifiers) extends FigureEvent

  case class MouseMove(screenPoint: ScreenPoint, modifiers: Key.Modifiers) extends FigureEvent

  case class Drag(button: MouseButton.MouseButton, screenPoint: ScreenPoint, modifiers: Key.Modifiers) extends FigureEvent

  case class Scroll(screenPoint: ScreenPoint, deltaX: Double, deltaY: Double, modifiers: Key.Modifiers) extends FigureEvent

}