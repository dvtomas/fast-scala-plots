package info.td.fasp.controller

import info.td.fasp.axis.{AxisXFactory, AxisYFactory}
import info.td.fasp.controller.PlotController.SetAxesDataRanges
import info.td.fasp.figurepart.{Axes, FigurePartKey}
import info.td.fasp.geom.{DataRange, ScreenRectangle}
import monifu.concurrent.Scheduler
import monifu.reactive.Observable
import monifu.reactive.subjects.PublishSubject

case class AxesOnScreen(key: FigurePartKey, figurePartBounds: ScreenRectangle, axes: Axes) {
  final def axesScreenBounds: ScreenRectangle = axes.screenBounds
}

case class AxesOnScreenSpec(
                             key: FigurePartKey,
                             computeFigurePartBounds: ScreenRectangle ⇒ ScreenRectangle,
                             computeAxesBounds: ScreenRectangle ⇒ ScreenRectangle,
                             axisXFactory: AxisXFactory, initialDataRangeX: DataRange,
                             axisYFactory: AxisYFactory, initialDataRangeY: DataRange
                           )

abstract class SetAxesDataRangesStrategy {
  def applySetAxesDataRanges(
                              setAxesDataRanges: SetAxesDataRanges,
                              axesOnScreenCollection: Seq[AxesOnScreen]
                            ): Seq[AxesOnScreen]
}

object SetAxesDataRangesStrategy {

  object Independent extends SetAxesDataRangesStrategy {
    override def applySetAxesDataRanges(
                                         setAxesDataRanges: SetAxesDataRanges,
                                         axesOnScreenCollection: Seq[AxesOnScreen]
                                       ): Seq[AxesOnScreen] = {
      axesOnScreenCollection.map(axesOnScreen ⇒
        if (axesOnScreen.key != setAxesDataRanges.key) {
          axesOnScreen
        } else {
          axesOnScreen.copy(axes = axesOnScreen.axes.withNewDataRanges(setAxesDataRanges.dataRangeX, setAxesDataRanges.dataRangeY))
        }
      )
    }
  }

  object LinkedXY extends SetAxesDataRangesStrategy {
    override def applySetAxesDataRanges(
                                         setAxesDataRanges: SetAxesDataRanges,
                                         axesOnScreenCollection: Seq[AxesOnScreen]
                                       ): Seq[AxesOnScreen] = {
      axesOnScreenCollection.map(axesOnScreen ⇒
        axesOnScreen.copy(axes = axesOnScreen.axes.withNewDataRanges(setAxesDataRanges.dataRangeX, setAxesDataRanges.dataRangeY))
      )
    }
  }

  object LinkedX extends SetAxesDataRangesStrategy {
    override def applySetAxesDataRanges(
                                         setAxesDataRanges: SetAxesDataRanges,
                                         axesOnScreenCollection: Seq[AxesOnScreen]
                                       ): Seq[AxesOnScreen] = {
      axesOnScreenCollection.map(axesOnScreen ⇒
        if (axesOnScreen.key != setAxesDataRanges.key) {
          axesOnScreen.copy(axes = axesOnScreen.axes.copy(axisX = axesOnScreen.axes.axisX.withNewDataRange(setAxesDataRanges.dataRangeX)))
        } else {
          axesOnScreen.copy(axes = axesOnScreen.axes.withNewDataRanges(setAxesDataRanges.dataRangeX, setAxesDataRanges.dataRangeY))
        }
      )
    }
  }

  object LinkedY extends SetAxesDataRangesStrategy {
    override def applySetAxesDataRanges(
                                         setAxesDataRanges: SetAxesDataRanges,
                                         axesOnScreenCollection: Seq[AxesOnScreen]
                                       ): Seq[AxesOnScreen] = {
      axesOnScreenCollection.map(axesOnScreen ⇒
        if (axesOnScreen.key != setAxesDataRanges.key) {
          axesOnScreen.copy(axes = axesOnScreen.axes.copy(axisY = axesOnScreen.axes.axisY.withNewDataRange(setAxesDataRanges.dataRangeY)))
        } else {
          axesOnScreen.copy(axes = axesOnScreen.axes.withNewDataRanges(setAxesDataRanges.dataRangeX, setAxesDataRanges.dataRangeY))
        }
      )
    }
  }

}

/** This class servers as a "holder" for the axes on screen. It subscribes to
  *
  * - Observable[FigureResizeEvent], when a figure resize event arrives, this class generates or updates the axes on screen from the appropriate specs
  * - Observable[SetAxesDataRanges], which are usually obtained from the FigureController, that has commands to change the data range of a particular axis on screen.
  *
  * @param figurePartSpecs - the specs that are used to create individual axes on screen
  * @param setAxesDataRangesStrategy - describes the way of handling the SetAxesDataRanges command. It can be used to link the axes of more plots together, for example. See concrete implementations of the strategies for examples.
  */
class AxesOnScreenSource(
                          figurePartSpecs: Seq[AxesOnScreenSpec],
                          setAxesDataRangesStrategy: SetAxesDataRangesStrategy
                        ) {
  private[this] val axesOnScreenSubject = PublishSubject[Seq[AxesOnScreen]]

  def subscribe(
                 figureResizeEventSource: Observable[FigureEvent.Resize],
                 setAxesDataRangesSource: Observable[SetAxesDataRanges]
               )(implicit scheduler: Scheduler) = {
    abstract sealed class What
    case class Resize(figureResizeEvent: FigureEvent.Resize) extends What
    case class SetRanges(setAxesDataRanges: SetAxesDataRanges) extends What

    val source =
      Observable.merge(
        figureResizeEventSource.map(Resize),
        setAxesDataRangesSource.map(SetRanges)
      ).scan(Seq.empty[AxesOnScreen]) { case (axesOnScreenCollection, what) ⇒
        what match {
          case Resize(resizeEvent) ⇒ {
            figurePartSpecs.map { plotSpec ⇒
              val newFigurePartBounds = plotSpec.computeFigurePartBounds(resizeEvent.screenRectangle)
              val newAxesBounds = plotSpec.computeAxesBounds(newFigurePartBounds)

              axesOnScreenCollection
                .find(_.key == plotSpec.key)
                .map { axesOnScreen ⇒
                  axesOnScreen.copy(
                    figurePartBounds = newFigurePartBounds,
                    axes = axesOnScreen.axes.withNewScreenBounds(newAxesBounds)
                  )
                }.getOrElse(
                AxesOnScreen(
                  plotSpec.key,
                  newFigurePartBounds,
                  Axes(
                    plotSpec.axisXFactory(newAxesBounds.horizontalRange, plotSpec.initialDataRangeX),
                    plotSpec.axisYFactory(newAxesBounds.verticalRange, plotSpec.initialDataRangeY))
                )
              )
            }
          }
          case SetRanges(setAxesDataRanges) ⇒ {
            setAxesDataRangesStrategy.applySetAxesDataRanges(setAxesDataRanges, axesOnScreenCollection)
          }
        }
      }
    source.onSubscribe(axesOnScreenSubject)(scheduler)
  }

  def axesOnScreenSource: Observable[Seq[AxesOnScreen]] = axesOnScreenSubject
}