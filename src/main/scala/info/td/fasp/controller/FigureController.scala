package info.td.fasp.controller

import monifu.reactive.Observable

class FigureController extends GenericFigureController(new PlotController) {
  final def setAxesDataRangesSource: Observable[PlotController.SetAxesDataRanges] =
    outputs.outputPlotControllerEventSource.collect {
      case x: PlotController.SetAxesDataRanges ⇒ x
    }

  final def maybePickRequestSource: Observable[Option[PlotController.PickRequest]] =
    (outputs.outputPlotControllerEventSource combineLatest outputs.maybePlotControllerStateSource) map {
      case (pick: PlotController.PickRequest, None) ⇒ Some(pick)
      case _ ⇒ None
    }
}