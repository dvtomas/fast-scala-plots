package info.td.fasp.controller

import info.td.common.monix.MonixHelpers._
import info.td.fasp.figurepart.FigurePartKey
import monifu.concurrent.Scheduler
import monifu.reactive.Observable
import monifu.reactive.subjects.PublishSubject

trait InputPlotControllerEvent {
  def isFor(axesOnScreen: AxesOnScreen): Boolean
}

trait OutputPlotControllerEvent

/** A plot controller to be plugged into a FigureController */
abstract class AbstractPlotController {
  type State

  type InputEvent <: InputPlotControllerEvent

  type OutputEvent <: OutputPlotControllerEvent

  /**
    * Should handle the fact that the axesOnScreen are requested to change (for example, the scrollBar wants to move the axes somewhere else).
    * @return Some new state if we want to hold the axes grabbed, and the axes on screen we selected/computed from the old and requested axes.
    */
  def handleNewAxesOnScreen(
                             state: State,
                             oldAxesOnScreen: AxesOnScreen,
                             requestedNewAxesOnScreen: AxesOnScreen
                           ): (Option[State], AxesOnScreen)

  /**
    * A controller event needs to be processed
    * @param axesOnScreen the state of the axes on the screen
    * @param maybeState If this controller has grabbed the axes on the screen, Some(grabbedState), else None
    * @param controllerEvent The controller event to process
    * @return If we want to grab the axes on the screen, Some(grabbedState), else None. If we also want to change the data ranges of the axes on screen, Some(new data ranges).
    */
  def handleControllerEvent(
                             axesOnScreen: AxesOnScreen,
                             maybeState: Option[State],
                             controllerEvent: InputEvent
                           ): (Option[State], Option[OutputEvent])
}

case class FigureControllerOutputs[PlotControllerType <: AbstractPlotController](
                                                                                  outputPlotControllerEventSource: Observable[OutputPlotControllerEvent],
                                                                                  maybePlotControllerStateSource: Observable[Option[PlotControllerType#State]]
                                                                                )

/** A generic figure controller with pluggable individual plot controller.
  *
  * Basically, it delivers events to the particular plot controller. There may be multiple axes on screen, and zero or one of them is "grabbed". Grabbed means that  there is an ongoing controller action with a particular axis on screen, e. g. dragging is in progress, or a zoom rectangle is being drawn. GenericFigureController takes care of delivering events to the currently grabbed axes on screen only, and managing the grabbed state. When a plot controller grabs an axis depends entirely on the controller itself.
  *
  * @param plotController The plot controller taking care of the handling of the events themselves
  */
class GenericFigureController[PlotControllerType <: AbstractPlotController](val plotController: PlotControllerType) {

  private[this] sealed abstract class FigureControllerEvent

  private[this] object FigureControllerEvent {

    case class NewAxesOnScreen(newAxesCollection: Seq[AxesOnScreen]) extends FigureControllerEvent

    case class ControllerEventArrived(controllerEvent: plotController.InputEvent) extends FigureControllerEvent

  }

  private[this] case class State(
                                  axesCollection: Seq[AxesOnScreen],
                                  grabbedControllerState: Option[(FigurePartKey, plotController.State)]
                                )

  private[this] val combinedOutputsSubject = PublishSubject[CombinedOutputs]()

  def subscribe(
                 controllerEventSource: Observable[plotController.InputEvent],
                 newAxesCollectionSource: Observable[Seq[AxesOnScreen]]
               )(implicit scheduler: Scheduler): Unit = {
    Observable.merge(
      newAxesCollectionSource.map(FigureControllerEvent.NewAxesOnScreen),
      controllerEventSource.map(FigureControllerEvent.ControllerEventArrived)
    ).scanMaybeResults[State, CombinedOutputs](State(Seq.empty, None))(collectFunc)
      .subscribe(combinedOutputsSubject)
  }

  private[this] case class CombinedOutputs(
                                            maybeOutputEvent: Option[PlotControllerType#OutputEvent],
                                            maybePlotControllerState: Option[PlotControllerType#State]
                                          )


  private[this] def collectFunc(state: State, event: FigureControllerEvent): (State, Option[CombinedOutputs]) =
    (state, event) match {
      case (State(_, None), FigureControllerEvent.NewAxesOnScreen(newAxesCollection)) ⇒ {
        (State(newAxesCollection, None), Some(CombinedOutputs(None, None)))
      }
      case (State(oldAxesOnScreenCollection, Some((grabbedAxesKey, grabbedControllerState))), FigureControllerEvent.NewAxesOnScreen(newAxesCollection)) ⇒ {
        val maybeNewStateAndNewGrabbedAxes =
          for (
            oldGrabbedAxes ← oldAxesOnScreenCollection.find(_.key == grabbedAxesKey);
            axesToUpdate ← newAxesCollection.find(_.key == grabbedAxesKey)
          ) yield plotController.handleNewAxesOnScreen(grabbedControllerState, oldGrabbedAxes, axesToUpdate)

        maybeNewStateAndNewGrabbedAxes match {
          case None ⇒ {
            // New axes don't even include the old grabbed one, just release it and use the new one
            (State(newAxesCollection, None), Some(CombinedOutputs(None, None)))
          }
          case Some((maybeControllerState, updatedGrabbedAxes)) ⇒ {
            // New axes included the grabbed one. We asked the controller what to do, and now we use that information to produce the final state..
            (
              State(
                newAxesCollection.map(axes ⇒ if (axes.key == grabbedAxesKey) updatedGrabbedAxes else axes),
                maybeControllerState.map(controllerState ⇒ (grabbedAxesKey, controllerState))
              ),
              Some(CombinedOutputs(None, None))
              )
          }
        }
      }
      case (State(axesCollection, None), FigureControllerEvent.ControllerEventArrived(plotControllerEvent)) ⇒ {
        axesCollection
          .find(plotControllerEvent.isFor)
          .map { axesOnScreen ⇒
            val (maybePlotControllerState, maybeOutputEvent) = plotController.handleControllerEvent(axesOnScreen, None, plotControllerEvent)
            (
              State(axesCollection, maybePlotControllerState.map(state ⇒ (axesOnScreen.key, state))),
              Some(CombinedOutputs(maybeOutputEvent, maybePlotControllerState))
              )
          }
          .getOrElse((State(axesCollection, None), Some(CombinedOutputs(None, None))))
      }
      case (State(axesCollection, Some(grabbedKeyAndController)), FigureControllerEvent.ControllerEventArrived(plotControllerEvent)) ⇒ {
        val grabbedController = grabbedKeyAndController._2
        axesCollection
          .find(axes ⇒ grabbedKeyAndController._1 == axes.key)
          .map { axesOnScreen ⇒
            val (maybePlotControllerState, maybeOutputEvent) =
              plotController.handleControllerEvent(axesOnScreen, Some(grabbedController), plotControllerEvent)
            (
              State(axesCollection, maybePlotControllerState.map(state ⇒ (axesOnScreen.key, state))),
              Some(CombinedOutputs(maybeOutputEvent, maybePlotControllerState))
              )
          }
          .getOrElse((State(axesCollection, Some(grabbedKeyAndController)), Some(CombinedOutputs(None, Some(grabbedController)))))
      }
    }

  final lazy val outputs: FigureControllerOutputs[PlotControllerType] =
    FigureControllerOutputs(
      combinedOutputsSubject.concatMapIterable(_.maybeOutputEvent),
      combinedOutputsSubject.map(_.maybePlotControllerState).distinctUntilChanged
    )
}

