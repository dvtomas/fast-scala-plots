package info.td.fasp.controller

import info.td.common.monix.MonixHelpers._
import monifu.reactive.Observable

object ControllerEventSourceDesktop {
  def controllerEventsSource(figureEventSource: Observable[FigureEvent]): Observable[PlotController.InputEvent] =
    figureEventSource.mergeMapIterable {
      case FigureEvent.Scroll(screenPoint, deltaX, deltaY, modifiers) ⇒ {
        val zoom = deltaY / -100.0
        val (zoomX, zoomY) = (modifiers.isShiftDown, modifiers.isControlDown) match {
          case (false, false) ⇒ (zoom, zoom)
          case (false, true) ⇒ (0.0, zoom)
          case (true, false) ⇒ (zoom, 0.0)
          case (true, true) ⇒ (zoom, zoom)
        }
        Seq(PlotController.Zoom(screenPoint, zoomX, zoomY))
      }
      case FigureEvent.MouseMove(screenPoint, _) ⇒ Seq(PlotController.PickRequest(screenPoint))
      case FigureEvent.Drag(MouseButton.Primary, screenPoint, _) ⇒ Seq(PlotController.ZoomRectangle(screenPoint))
      case FigureEvent.Release(MouseButton.Primary, screenPoint, _) ⇒ Seq(PlotController.ZoomRectangleConfirm(screenPoint))
      case FigureEvent.Drag(MouseButton.Middle, screenPoint, _) ⇒ Seq(PlotController.Drag(screenPoint))
      case FigureEvent.Release(MouseButton.Middle, screenPoint, _) ⇒ Seq(PlotController.DragConfirm(screenPoint))
      case FigureEvent.KeyPressed(Key.Escape) ⇒ Seq(PlotController.CancelAction)
      case _ ⇒ Seq.empty
    }
}