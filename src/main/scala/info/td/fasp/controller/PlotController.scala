package info.td.fasp.controller

import info.td.fasp.figurepart.{Axes, FigurePartKey}
import info.td.fasp.geom.{DataPoint, DataRange, ScreenPoint, ScreenRectangle}

object PlotController {

  // -- State

  abstract sealed class State

  case class DragInProgress(dragStartAxesOnScreen: AxesOnScreen, dragStartDataPoint: DataPoint) extends State {
    def draggedAxes(dragPoint: ScreenPoint) = {
      val axes = dragStartAxesOnScreen.axes
      val newAxes = Axes(
        axes.axisX.dragged(dragStartDataPoint.x, axes.axisX.dataValue(dragPoint.x)),
        axes.axisY.dragged(dragStartDataPoint.y, axes.axisY.dataValue(dragPoint.y))
      )
      dragStartAxesOnScreen.copy(axes = newAxes)
    }
  }

  case class ZoomRectangleInProgress(origin: ScreenPoint, corner: ScreenPoint) extends State

  // -- Input events

  sealed trait InputEvent extends InputPlotControllerEvent {
    protected[this] def hits(screenPoint: ScreenPoint, axesOnScreen: AxesOnScreen): Boolean =
      axesOnScreen.axesScreenBounds.isHit(screenPoint)
  }

  case class Drag(screenPoint: ScreenPoint) extends InputEvent {
    override def isFor(axesOnScreen: AxesOnScreen): Boolean = hits(screenPoint, axesOnScreen)
  }

  case class DragConfirm(screenPoint: ScreenPoint) extends InputEvent {
    override def isFor(axesOnScreen: AxesOnScreen): Boolean = hits(screenPoint, axesOnScreen)
  }

  case class ZoomRectangle(screenPoint: ScreenPoint) extends InputEvent {
    override def isFor(axesOnScreen: AxesOnScreen): Boolean = hits(screenPoint, axesOnScreen)
  }

  case class ZoomRectangleConfirm(screenPoint: ScreenPoint) extends InputEvent {
    override def isFor(axesOnScreen: AxesOnScreen): Boolean = hits(screenPoint, axesOnScreen)
  }

  case class Zoom(screenPoint: ScreenPoint, zoomX: Double, zoomY: Double) extends InputEvent {
    override def isFor(axesOnScreen: AxesOnScreen): Boolean = hits(screenPoint, axesOnScreen)
  }

  case class PickRequest(screenPoint: ScreenPoint) extends InputEvent with OutputEvent {
    override def isFor(axesOnScreen: AxesOnScreen): Boolean = hits(screenPoint, axesOnScreen)
  }

  case object CancelAction extends InputEvent {
    override def isFor(axesOnScreen: AxesOnScreen): Boolean = true
  }

  // -- Output events

  sealed trait OutputEvent extends OutputPlotControllerEvent

  case class SetAxesDataRanges(key: FigurePartKey, dataRangeX: DataRange, dataRangeY: DataRange) extends OutputEvent

}

class PlotController extends AbstractPlotController {

  import PlotController._

  override type State = PlotController.State

  override type InputEvent = PlotController.InputEvent

  override type OutputEvent = PlotController.OutputEvent

  override def handleControllerEvent(
                                      axesOnScreen: AxesOnScreen,
                                      maybeState: Option[State],
                                      controllerEvent: InputEvent
                                    ): (Option[State], Option[OutputEvent]) = {
    def key = axesOnScreen.key

    def setAxesDataRanges(axesOnScreen: AxesOnScreen) = {
      SetAxesDataRanges(key, axesOnScreen.axes.axisX.dataRange, axesOnScreen.axes.axisY.dataRange)
    }

    controllerEvent match {
      case pick: PickRequest ⇒ (maybeState, Some(pick))

      case Zoom(screenPoint, zoomX, zoomY) ⇒ {
        maybeState match {
          case None ⇒ (None, Some(
            SetAxesDataRanges(
              key,
              axesOnScreen.axes.axisX.zoomed(screenPoint.x, zoomX).dataRange,
              axesOnScreen.axes.axisY.zoomed(screenPoint.y, zoomY).dataRange
            )
          ))
          case _ ⇒ (maybeState, None)
        }
      }

      case ZoomRectangle(screenPoint) ⇒ {
        maybeState match {
          case None ⇒ (Some(ZoomRectangleInProgress(screenPoint, screenPoint)), None)
          case Some(ZoomRectangleInProgress(origin, _)) ⇒ {
            val newCorner = axesOnScreen.axes.screenBounds.boundScreenPoint(screenPoint)
            (Some(ZoomRectangleInProgress(origin, newCorner)), None)
          }
          case _ ⇒ (maybeState, None)
        }
      }

      case ZoomRectangleConfirm(screenPoint) ⇒ {
        def mapAxes(axesOnScreen: AxesOnScreen)(f: Axes ⇒ Axes): AxesOnScreen = {
          axesOnScreen.copy(axes = f(axesOnScreen.axes))
        }

        maybeState match {
          case Some(ZoomRectangleInProgress(origin, _)) ⇒ {
            val newCorner = axesOnScreen.axes.screenBounds.boundScreenPoint(screenPoint)
            val zoomRectangle = ScreenRectangle(origin, newCorner)
            val minimumZoomRectangleSide = 5
            if (zoomRectangle.width >= minimumZoomRectangleSide && zoomRectangle.height >= minimumZoomRectangleSide) {
              val newAxesOnScreen = mapAxes(axesOnScreen) { axes ⇒
                val (axisX, axisY) = (axes.axisX, axes.axisY)
                axes.withNewDataRanges(
                  DataRange(axisX.dataValue(zoomRectangle.left), axisX.dataValue(zoomRectangle.right)),
                  DataRange(axisY.dataValue(zoomRectangle.bottom), axisY.dataValue(zoomRectangle.top))
                )
              }
              (None, Some(setAxesDataRanges(newAxesOnScreen)))
            } else {
              (None, None)
            }
          }
          case _ ⇒ (maybeState, None)
        }
      }

      case Drag(screenPoint) ⇒ {
        maybeState match {
          case None ⇒ (Some(DragInProgress(axesOnScreen, axesOnScreen.axes.dataPoint(screenPoint))), None)
          case Some(dragInProgress: DragInProgress) ⇒ {
            val draggedAxes = dragInProgress.draggedAxes(screenPoint)
            (Some(dragInProgress), Some(setAxesDataRanges(draggedAxes)))
          }
          case _ ⇒ (maybeState, None)
        }
      }

      case DragConfirm(screenPoint) ⇒ {
        maybeState match {
          case Some(dragInProgress: DragInProgress) ⇒ {
            val draggedAxes = dragInProgress.draggedAxes(screenPoint)
            (None, Some(setAxesDataRanges(draggedAxes)))
          }
          case _ ⇒ (maybeState, None)
        }
      }

      case CancelAction ⇒ {
        maybeState match {
          case Some(DragInProgress(dragStartAxesOnScreen, _)) ⇒ (None, Some(setAxesDataRanges(dragStartAxesOnScreen)))
          case _ ⇒ (None, None)
        }
      }
    }
  }

  override def handleNewAxesOnScreen(
                                      state: State,
                                      oldAxesOnScreen: AxesOnScreen,
                                      newAxesOnScreen: AxesOnScreen
                                    ): (Option[State], AxesOnScreen) = {
    def haveOnlyDataRangesChanged: Boolean = {
      oldAxesOnScreen.key == newAxesOnScreen.key &&
        oldAxesOnScreen.figurePartBounds == newAxesOnScreen.figurePartBounds &&
        oldAxesOnScreen.axesScreenBounds == newAxesOnScreen.axesScreenBounds &&
        oldAxesOnScreen.axes.axisX.getClass == newAxesOnScreen.axes.axisX.getClass &&
        oldAxesOnScreen.axes.axisY.getClass == newAxesOnScreen.axes.axisY.getClass
    }

    def acceptNewIfOnlyDataRangesHaveChanged =
      if (haveOnlyDataRangesChanged) {
        (Some(state), newAxesOnScreen)
      } else {
        (None, newAxesOnScreen)
      }

    state match {
      case DragInProgress(dragStartAxesOnScreen, dragStartDataPoint) ⇒ acceptNewIfOnlyDataRangesHaveChanged
      case ZoomRectangleInProgress(origin, corner) ⇒ acceptNewIfOnlyDataRangesHaveChanged
    }
  }
}