package info.td.fasp.figure

import info.td.common.monix.MonixHelpers._
import info.td.fasp.controller.AxesOnScreen
import info.td.fasp.figurepart.{Axes, FigurePart, FigurePartKey, FigurePartProperties}
import info.td.fasp.geom.ScreenRectangle
import info.td.fasp.plot.Plot
import monifu.concurrent.Scheduler
import monifu.reactive.Observable
import monifu.reactive.subjects.ReplaySubject

class Figure private(figurePartsMap: Map[FigurePartKey, FigurePart]) {
  def figureParts = figurePartsMap.values.toSeq

  final def updated(figurePart: FigurePart): Figure =
    new Figure(figurePartsMap.updated(figurePart.key, figurePart))
}

object Figure {
  def empty = new Figure(Map.empty)

  def apply(figureParts: Seq[FigurePart]) =
    new Figure(figureParts.map(figurePart ⇒ figurePart.key → figurePart).toMap)
}

class FigureSource {

  private[this] case class UpdateResult(changedFigureParts: Map[FigurePartKey, FigurePart], figure: Figure) {
    def withUpdatedFigurePart(figurePart: FigurePart): UpdateResult = {
      UpdateResult(changedFigureParts.updated(figurePart.key, figurePart), figure.updated(figurePart))
    }
  }

  private[this] val updateResultSubject = ReplaySubject.createWithSize[UpdateResult](1)

  private[this] abstract sealed class Update

  private[this] case class AxesOnScreenUpdate(axesOnScreenSeq: Seq[AxesOnScreen]) extends Update

  private[this] case class PropertiesUpdate(newProperties: Map[FigurePartKey, FigurePartProperties]) extends Update

  private[this] case class PlotUpdate(newPlots: Map[FigurePartKey, Seq[Plot]]) extends Update

  def subscribe(
                 axesOnScreenSource: Observable[Seq[AxesOnScreen]],
                 propertiesSource: Observable[Map[FigurePartKey, FigurePartProperties]],
                 plotUpdateSource: Observable[Map[FigurePartKey, Seq[Plot]]]
               )(implicit scheduler: Scheduler): Unit = {

    val updatesSource = Observable.merge(
      axesOnScreenSource.map(AxesOnScreenUpdate),
      propertiesSource.map(PropertiesUpdate),
      plotUpdateSource.map(PlotUpdate)
    )

    def updateFromAxesOnScreen(
                                oldUpdateResult: UpdateResult,
                                newAxesOnScreenCollection: Seq[AxesOnScreen]
                              ): UpdateResult = {
      newAxesOnScreenCollection.foldLeft(oldUpdateResult) { case (updateResult, axesOnScreen) ⇒
        updateResult.figure.figureParts
          .find(_.key == axesOnScreen.key)
          .map(updateResult withUpdatedFigurePart _.withNewAxesOnScreen(axesOnScreen))
          .getOrElse {
            updateResult withUpdatedFigurePart FigurePart(
              axesOnScreen.key,
              axesOnScreen.figurePartBounds,
              axesOnScreen.axes,
              Seq.empty,
              FigurePartProperties.empty
            )
          }
      }
    }

    def updateFromProperties(
                              oldUpdateResult: UpdateResult,
                              newPropertiesMap: Map[FigurePartKey, FigurePartProperties]
                            ): UpdateResult = {
      newPropertiesMap.foldLeft(oldUpdateResult) { case (updateResult, (figurePartKey, properties)) ⇒
        updateResult.figure.figureParts
          .find(_.key == figurePartKey)
          .map(updateResult withUpdatedFigurePart _.copy(properties = properties))
          .getOrElse {
            updateResult withUpdatedFigurePart FigurePart(
              figurePartKey,
              ScreenRectangle.zero,
              Axes.empty,
              Seq.empty,
              properties
            )
          }
      }
    }

    def updateFromPlots(
                         oldUpdateResult: UpdateResult,
                         newPlotsMap: Map[FigurePartKey, Seq[Plot]]
                       ): UpdateResult = {
      newPlotsMap.foldLeft(oldUpdateResult) { case (updateResult, (figurePartKey, plots)) ⇒
        updateResult.figure.figureParts
          .find(_.key == figurePartKey)
          .map(updateResult withUpdatedFigurePart _.withUpdatedPlots(plots))
          .getOrElse {
            updateResult withUpdatedFigurePart FigurePart(
              figurePartKey,
              ScreenRectangle.zero,
              Axes.empty,
              plots,
              FigurePartProperties.empty
            )
          }
      }
    }

    updatesSource
      .scan(UpdateResult(Map.empty, Figure.empty)) { case (oldUpdateResult, update) ⇒
        update match {
          case AxesOnScreenUpdate(axesOnScreenCollection) ⇒ updateFromAxesOnScreen(oldUpdateResult, axesOnScreenCollection)
          case PropertiesUpdate(newProperties) ⇒ updateFromProperties(oldUpdateResult, newProperties)
          case PlotUpdate(newPlots) ⇒ updateFromPlots(oldUpdateResult, newPlots)
        }
      }.onSubscribe(updateResultSubject)
  }

  def figureSource: Observable[Figure] = updateResultSubject.map(_.figure)

  def changedFigurePartSource: Observable[FigurePart] =
    updateResultSubject.map(_.changedFigureParts.values).concatMapIterable(_.toSeq)
}


