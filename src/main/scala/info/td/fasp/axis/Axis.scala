package info.td.fasp.axis

import info.td.fasp.geom._

trait Axis {
  type AxisType <: Axis

  def screenRange: ScreenRange

  def dataRange: DataRange

  def newInstance(screenRange: ScreenRange, dataRange: DataRange): AxisType

  def withNewScreenRange(newScreenRange: ScreenRange): AxisType =
    if (newScreenRange != screenRange) {
      newInstance(newScreenRange, this.dataRange)
    } else {
      this.asInstanceOf[AxisType]
    }

  def withNewDataRange(newDataRange: DataRange): AxisType =
  if (newDataRange != dataRange) {
    newInstance(this.screenRange, newDataRange)
  } else {
    this.asInstanceOf[AxisType]
  }

  def dataValue(screenValue: Double): Double

  def screenValue(dataValue: Double): Double

  /*
  private def findDataIndicesRangeInPixelAscending(range: EquidistantRange, pixel: Int): Option[Range] = {
    if (screenValue(range.value(range.numberOfPoints - 1)) <= pixel)
      return None
    if (screenValue(range.value(0)) >= pixel + 1)
      return None
    // Now we know that at least one point is included in this pixel. That means we'll be returning a Some.
    val dataLow = dataValue(pixel)
    val dataHigh = dataValue(pixel + 1)
    val potentialLowIndex = range.uncheckedIndexOfPointLeftOf(dataLow) max 0
    val lowIndex = if (range.value(potentialLowIndex) < dataLow)
      potentialLowIndex + 1
    else
      potentialLowIndex
    val highIndex = range.uncheckedIndexOfPointLeftOf(dataHigh) min (range.numberOfPoints - 1)
    Some(lowIndex to highIndex)
  }

  private def findDataIndicesRangeInPixelDescending(range: EquidistantRange, pixel: Int): Option[Range] = {
    if (screenValue(range.value(range.numberOfPoints - 1)) >= pixel + 1)
      return None
    if (screenValue(range.value(0)) <= pixel)
      return None
    // Now we know that at least one point is included in this pixel. That means we'll be returning a Some.
    val dataLow = dataValue(pixel + 1)
    val dataHigh = dataValue(pixel)
    val potentialLowIndex = range.uncheckedIndexOfPointLeftOf(dataLow) max 0
    val lowIndex = if (range.value(potentialLowIndex) < dataLow)
      potentialLowIndex + 1
    else
      potentialLowIndex
    val highIndex = range.uncheckedIndexOfPointLeftOf(dataHigh) min (range.numberOfPoints - 1)
    Some(lowIndex to highIndex)
  }

  def findDataIndicesPackedByPixels(range: EquidistantRange): Seq[EquidistantRange.DataIndicesAtPixel] = {
    if (range.numberOfPoints == 0) {
      Seq.empty
    } else if (range.numberOfPoints == 1) {
      Seq(EquidistantRange.DataIndicesAtPixel(screenValue(range.value(0)).toInt, 0 to 0))
    } else {
      val wholePixelsRange = if (screenRange.minimum < screenRange.maximum)
        screenRange.minimum.round.toInt until screenRange.maximum.round.toInt
      else
        screenRange.maximum.round.toInt until screenRange.minimum.round.toInt

      if (screenValue(range.value(1)) > screenValue(range.value(0))) {
        for (
          pixel ← wholePixelsRange;
          dataIndices ← findDataIndicesRangeInPixelAscending(range, pixel)
        ) yield EquidistantRange.DataIndicesAtPixel(pixel, dataIndices)
      } else {
        for (
          pixel ← wholePixelsRange.reverse;
          dataIndices ← findDataIndicesRangeInPixelDescending(range, pixel)
        ) yield EquidistantRange.DataIndicesAtPixel(pixel, dataIndices)
      }
    }
  }
  */

  def zoomed(screenCenter: Double, zoom: Double): AxisType = {
    val newMinimum = screenCenter - (screenCenter - screenValue(dataRange.minimum)) * (1 + zoom)
    val newMaximum = screenCenter + ((screenValue(dataRange.maximum) - screenCenter) * (1 + zoom))
    withNewDataRange(DataRange(dataValue(newMinimum), dataValue(newMaximum)))
  }

  def dragged(oldData: Double, newData: Double): AxisType = {
    val screenOffset = screenValue(newData) - screenValue(oldData)
    def translatedDataValue(dv: Double) = dataValue(screenValue(dv) - screenOffset)
    withNewDataRange(DataRange(translatedDataValue(dataRange.minimum), translatedDataValue(dataRange.maximum)))
  }
}

trait AxisX extends Axis {
  type AxisType <: AxisX
}

trait AxisY extends Axis {
  type AxisType <: AxisY
}

trait LinearAxisAscending extends Axis {
  private[this] val (k, q) = {
    if (screenRange.length == 0.0 || dataRange.length == 0.0) {
      (1.0, 0.0)
    } else {
      val k = dataRange.length / screenRange.length
      val q = k * screenRange.minimum - dataRange.minimum
      (k, q)
    }
  }

  def dataValue(screenValue: Double) = k * screenValue - q

  def screenValue(dataValue: Double) = (dataValue + q) / k
}

trait LinearAxisDescending extends Axis {
  private[this] val (k, q) = {
    if (screenRange.length == 0.0 || dataRange.length == 0.0) {
      (1.0, 0.0)
    } else {
      val k = dataRange.length / -screenRange.length
      val q = k * screenRange.maximum - dataRange.minimum
      (k, q)
    }
  }

  def dataValue(screenValue: Double) = k * screenValue - q

  def screenValue(dataValue: Double) = (dataValue + q) / k
}

abstract class AxisXFactory {
  def apply(screenRange: ScreenRange, dataRange: DataRange): AxisX
}

abstract class AxisYFactory {
  def apply(screenRange: ScreenRange, dataRange: DataRange): AxisY
}

case object LinearAxisXFactory extends AxisXFactory {
  override def apply(screenRange: ScreenRange, dataRange: DataRange): AxisX = LinearAxisX(screenRange, dataRange)
}

case object LinearAxisYFactory extends AxisYFactory {
  override def apply(screenRange: ScreenRange, dataRange: DataRange): AxisY = LinearAxisY(screenRange, dataRange)
}

case class LinearAxisX(screenRange: ScreenRange, dataRange: DataRange) extends LinearAxisAscending with AxisX {
  override type AxisType = LinearAxisX

  override def newInstance(screenRange: ScreenRange, dataRange: DataRange): AxisType =
    LinearAxisX(ScreenRange(screenRange.minimum, screenRange.maximum), dataRange)
}

case class LinearAxisY(screenRange: ScreenRange, dataRange: DataRange) extends LinearAxisDescending with AxisY {
  override type AxisType = LinearAxisY

  override def newInstance(screenRange: ScreenRange, dataRange: DataRange): AxisType =
    LinearAxisY(screenRange, dataRange)
}