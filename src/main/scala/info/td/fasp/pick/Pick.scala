package info.td.fasp.pick

import info.td.fasp.controller.PlotController
import info.td.fasp.controller.PlotController.PickRequest
import info.td.fasp.figure.Figure
import info.td.fasp.figurepart.FigurePartKey
import info.td.fasp.geom.DataPoint
import info.td.fasp.plot.PlotKey
import monifu.concurrent.Scheduler
import monifu.reactive.Observable

abstract class PickedPoint {
  def plotKey: PlotKey
}

case class XYPickedPoint(plotKey: PlotKey, pointIndex: Int) extends PickedPoint

case class HeightMapPickedPoint(
                                 plotKey: PlotKey,
                                 row: Int, column: Int
                               ) extends PickedPoint

case class PickerPosition(
                           dataPoint: DataPoint,
                           strategy: PickStrategy
                         )

case class PickResult(
                       figurePartKey: FigurePartKey,
                       pickerPositions: Seq[PickerPosition],
                       pickedPoints: Seq[PickedPoint]
                     )

abstract class PickerLinkStrategy {
  def pickAlsoAt(pickResult: PickResult, figure: Figure): Seq[PlotController.PickRequest]
}

object PickerLinkStrategy {

  object None extends PickerLinkStrategy {
    override def pickAlsoAt(pickResult: PickResult, figure: Figure): Seq[PickRequest] = Seq.empty
  }

  object SameDataPoints extends PickerLinkStrategy {
    override def pickAlsoAt(pickResult: PickResult, figure: Figure): Seq[PickRequest] = {
      for (
        pickerPosition ← pickResult.pickerPositions;
        figurePart ← figure.figureParts if figurePart.key != pickResult.figurePartKey
      ) yield PickRequest(figurePart.axes.screenPoint(pickerPosition.dataPoint))
    }
  }

}

object PickResult {
  def empty(figurePartKey: FigurePartKey) = PickResult(figurePartKey, Seq.empty, Seq.empty)

  def pickResultSource(
                        picker: FigurePartPicker,
                        maybePickRequestSource: Observable[Option[PlotController.PickRequest]],
                        figureSource: Observable[Figure],
                        pickerLinkStrategy: PickerLinkStrategy = PickerLinkStrategy.None
                      )(implicit scheduler: Scheduler): Observable[Seq[PickResult]] = {
    import info.td.common.monix.MonixHelpers._

    (maybePickRequestSource combineLatest figureSource).observeLatest
      .map { case (maybePickRequest, figure) ⇒
        val originalPickResults = maybePickRequest.toSeq.flatMap(pickRequest ⇒
          figure.figureParts
            .filter(figurePart ⇒ pickRequest.isFor(figurePart.axesOnScreen))
            .map(picker.pick(pickRequest, _))
        )
        val linkedPickRequests = originalPickResults.flatMap(pickerLinkStrategy.pickAlsoAt(_, figure))
        val linkedPickResults = linkedPickRequests.flatMap(pickRequest ⇒ figure.figureParts.map(picker.pick(pickRequest, _)))
        originalPickResults ++ linkedPickResults
      }
  }
}