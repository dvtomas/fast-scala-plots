package info.td.fasp.pick

import com.typesafe.scalalogging.LazyLogging
import info.td.fasp.controller.PlotController
import info.td.fasp.figurepart.FigurePart

class FigurePartPicker(val strategy: PickStrategy, plotPickersSpec: Seq[PlotPicker]) {
  def pick(pickRequest: PlotController.PickRequest, figurePart: FigurePart): PickResult = {
    val pickedPoints =
      for (
        plotPicker ← plotPickersSpec;
        plot ← figurePart.plots;
        points ← plotPicker.pickAt(figurePart.key, pickRequest.screenPoint, figurePart.axes, plot)
      ) yield points

    PickResult(
      figurePart.key,
      Seq(PickerPosition(figurePart.axes.dataPoint(pickRequest.screenPoint), strategy)),
      pickedPoints
    )
  }
}

object FigurePartPicker extends LazyLogging {
  def nonePicker = new FigurePartPicker(PickStrategy.None, Seq.empty)

  def xyPicker = new FigurePartPicker(PickStrategy.XY, Seq(new XYNearestPlotPicker))

  def verticalPicker = new FigurePartPicker(PickStrategy.Vertical, Seq(new VerticalPlotPicker))

  def apply(strategy: PickStrategy) = strategy match {
    case PickStrategy.None ⇒ nonePicker
    case PickStrategy.XY ⇒ xyPicker
    case PickStrategy.Vertical ⇒ verticalPicker
    case _ ⇒ {
      logger.error(s"Unknown PickStrategy: $strategy")
      nonePicker
    }
  }
}