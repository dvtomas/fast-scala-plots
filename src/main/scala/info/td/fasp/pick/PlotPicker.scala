package info.td.fasp.pick

import info.td.fasp.figurepart.{Axes, FigurePartKey}
import info.td.fasp.geom.{DataPoint, DataRange, ScreenPoint}
import info.td.fasp.plot.{Plot, XYPlot}

abstract class PlotPicker {
  def pickAt(
              figurePartKey: FigurePartKey,
              where: ScreenPoint,
              axes: Axes,
              plot: Plot
            ): Seq[PickedPoint]
}

class XYNearestPlotPicker extends PlotPicker {
  override def pickAt(figurePartKey: FigurePartKey, where: ScreenPoint, axes: Axes, plot: Plot): Seq[PickedPoint] = {
    plot match {
      case xyPlot: XYPlot ⇒ {
        def square(x: Double): Double = x * x
        def squaredDistance(point: DataPoint) = square((axes.screenPoint(point).x - where.x).abs) + square((axes.screenPoint(point).y - where.y).abs)
        val xDataRange: DataRange = axes.axisX.dataRange
        val yDataRange = axes.axisY.dataRange
        val pointsToExamineWithIndices =
          xyPlot
            .data.iterateValues
            .zipWithIndex
            .filter { case (point, _) ⇒ (xDataRange contains point.x) && (yDataRange contains point.y) }

        if (pointsToExamineWithIndices.isEmpty)
          Seq.empty
        else {
          val closestPointWithIndex = pointsToExamineWithIndices minBy (pointWithIndex ⇒ squaredDistance(pointWithIndex._1))
          val distance = Math.sqrt(squaredDistance(closestPointWithIndex._1))
          if (distance < 20)
            Seq(XYPickedPoint(xyPlot.plotKey, closestPointWithIndex._2))
          else
            Seq.empty
        }
      }
    }
  }
}

class VerticalPlotPicker extends PlotPicker {
  /** First tries to find closest point on the same pixel column, then looks in the neighbourhood */
  override def pickAt(figurePartKey: FigurePartKey, where: ScreenPoint, axes: Axes, plot: Plot): Seq[PickedPoint] = {
    plot match {
      case xyPlot: XYPlot ⇒ {

        def horizontalDistance(point: DataPoint) = (axes.screenPoint(point).x - where.x).abs
        def verticalDistance(point: DataPoint) = (axes.screenPoint(point).y - where.y).abs
        val yDataRange = axes.axisY.dataRange
        val candidatePointsWithIndices =
          xyPlot
            .data.iterateValues
            .zipWithIndex
            .filter { case (point, _) ⇒ yDataRange.contains(point.y) && horizontalDistance(point) < 10 }
            .toList

        if (candidatePointsWithIndices.isEmpty) {
          Seq.empty
        } else {
          val pointsWithIndicesOnTheSameVertical = candidatePointsWithIndices.filter { case (point, _) ⇒ horizontalDistance(point) <= 1 }
          val closestPointWithIndex = if (pointsWithIndicesOnTheSameVertical.isEmpty) {
            candidatePointsWithIndices minBy { case (point, _) ⇒ horizontalDistance(point) }
          } else {
            pointsWithIndicesOnTheSameVertical minBy { case (point, _) ⇒ verticalDistance(point) }
          }
          Seq(XYPickedPoint(xyPlot.plotKey, closestPointWithIndex._2))
        }
      }
    }
  }
}