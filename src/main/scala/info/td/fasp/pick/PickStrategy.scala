package info.td.fasp.pick

abstract class PickStrategy

object PickStrategy {

  case object None extends PickStrategy

  case object XY extends PickStrategy

  case object Vertical extends PickStrategy

}
