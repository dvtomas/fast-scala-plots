package info.td.fasp.geom

import info.td.fasp.javafx.JavaFXConversions.DoubleHelpers

trait DoublePoint {
  def x: Double

  def y: Double


}

abstract sealed class AbstractScreenPoint

case object InvalidScreenPoint extends AbstractScreenPoint

case class ScreenPoint(x: Double, y: Double) extends AbstractScreenPoint with DoublePoint {
  def snappedToPixelCenter = ScreenPoint(x.snappedToPixelCenter, y.snappedToPixelCenter)
}

abstract sealed class AbstractDataPoint

case class DataPoint(x: Double, y: Double) extends AbstractDataPoint with DoublePoint