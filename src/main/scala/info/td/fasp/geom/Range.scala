package info.td.fasp.geom

import com.typesafe.scalalogging.LazyLogging

object AbstractRange extends LazyLogging {
  def warn(s: String): Unit = {
    logger.warn(s)
  }
}

abstract class AbstractRange {
  type ThisType <: AbstractRange

  def minimum: Double

  def maximum: Double

  protected[this] def assertMinimumIsLessThenMaximum(): Unit = {
    if (maximum < minimum)
      AbstractRange.warn(s"minimum > maximum: $this")
  }

  def newInstance(minimum: Double, maximum: Double): ThisType

  def length = maximum - minimum

  def isAscending = minimum <= maximum

  def -(offset: Double) = newInstance(minimum - offset, maximum - offset)

  //  def isValidDataRange = !minimum.isNaN && !minimum.isInfinity && !maximum.isNaN && !maximum.isInfinity

  /*
    def union(other: ThisType) = if (isAscending)
      newInstance(MathUtils.minimumWithNaNs(minimum, other.minimum), MathUtils.maximumWithNaNs(maximum, other.maximum))
    else
      unionDescending(other)
  */

  /*
    def union(point: Double) = if (isAscending)
      newInstance(MathUtils.minimumWithNaNs(minimum, point), MathUtils.maximumWithNaNs(maximum, point))
    else
      newInstance(MathUtils.maximumWithNaNs(minimum, point), MathUtils.minimumWithNaNs(maximum, point))
  */

  /*
    private[this] def unionDescending(other: ThisType) = newInstance(MathUtils.maximumWithNaNs(minimum, other.minimum), MathUtils.minimumWithNaNs(maximum, other.maximum))
  */

  /*
    private[this] def intersectionAscending(other: ThisType) = {
      if (!(minimum <= other.maximum && other.minimum <= maximum)) // this logic also handles NaN's, change with extreme care!
        newInstance(Double.NaN, Double.NaN)
      else
        newInstance(minimum max other.minimum, maximum min other.maximum)
    }
  */

  def contains(value: Double) =
    if (isAscending)
      value >= minimum && value <= maximum
    else
      value >= maximum && value <= minimum

  def intersectsWith(that: ThisType) = {
    val min1 = this.minimum min this.maximum
    val max1 = this.minimum max this.maximum
    val min2 = that.minimum min that.maximum
    val max2 = that.minimum max that.maximum
    val outside = (min1 >= max2) || (max1 < min2)
    !outside
  }

  def translatePositionToPositionOn(value: Double, anotherRange: ThisType) = {
    val ratio = (value - minimum) / (maximum - minimum)
    ratio * (anotherRange.maximum - anotherRange.minimum) + anotherRange.minimum
  }

  def center = (maximum + minimum) / 2

  def symmetricValue(value: Double) = (2 * center) - value

  def ascending = if (minimum <= maximum) this else newInstance(maximum, minimum)

  def map(f: Double ⇒ Double) = newInstance(f(minimum), f(maximum))

  /*
    def expandToNearestTenth = {
      val exponent = MathUtils.decadicExponent(length.abs)
      val multiplier = Math.pow(10.0, exponent - 1)
      val result = if (isAscending)
        newInstance(Math.floor(minimum / multiplier) * multiplier, Math.ceil(maximum / multiplier) * multiplier)
      else
        newInstance(Math.ceil(minimum / multiplier) * multiplier, Math.floor(maximum / multiplier) * multiplier)
      result
    }
  */

  /*
    def expandBy(amount: Double) = {
      if (isAscending) {
        newInstance(minimum - amount, maximum + amount)
      } else {
        newInstance(minimum + amount, maximum - amount)
      }
    }
  */
}


case class DataRange(minimum: Double, maximum: Double) extends AbstractRange {
  type ThisType = DataRange

  assertMinimumIsLessThenMaximum()

  def newInstance(minimum: Double, maximum: Double) = DataRange(minimum, maximum)
}

case class ScreenRange(minimum: Double, maximum: Double) extends AbstractRange {
  type ThisType = ScreenRange

  assertMinimumIsLessThenMaximum()

  def newInstance(minimum: Double, maximum: Double) = ScreenRange(minimum, maximum)
}