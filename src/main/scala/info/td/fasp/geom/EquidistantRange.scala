package info.td.fasp.geom

object EquidistantRange {

  /**
    *
    * @param pixel pixel at screen, pixel + 0.5 is the center of the pixel
    * @param dataIndices Indices of the data provider that fall in that pixel
    */
  case class DataIndicesAtPixel(pixel: Int, dataIndices: Range)

}

case class EquidistantRange(start: Double, numberOfPoints: Int, step: Double) {

  final def value(index: Int): Double = start + step * index

  def uncheckedIndexOfPointLeftOf(x: Double): Int = ((x - start) / step).toInt

  /** Returns a range of indices of points that are contained in range, and also one point on each side before the start and after the end of the range. If the provider
    * doesn't have enough points, the result will only contain indices of valid points, even though they may not cover the whole requested range. */
  def indicesOfPointsJustAround(range: DataRange): Range = {
    if (numberOfPoints == 0) {
      0 until 0
    } else {
      val index1 = uncheckedIndexOfPointLeftOf(range.minimum)
      val index2 = uncheckedIndexOfPointLeftOf(range.maximum)
      if (index1 < index2) {
        (index1 max 0) to ((index2 + 1) min (numberOfPoints - 1))
      } else {
        (index2 max 0) to ((index1 + 1) min (numberOfPoints - 1))
      }
    }
  }

  /** Just like `indicesOfPointsJustAround`, but doesn't care if the points are contained in the range */
  def uncheckedIndicesOfPointsJustAround(range: DataRange): Range = {
    val index1 = uncheckedIndexOfPointLeftOf(range.minimum)
    val index2 = uncheckedIndexOfPointLeftOf(range.maximum)
    index1 to index2
  }

  def iterateIndices(indices: Range): Iterator[Int] =
    if (indices.isEmpty) {
      Iterator.empty
    } else {
      new Iterator[Int] {
        private var index: Int = indices.min

        def hasNext = indices.contains(index)

        def next() = {
          val oldIndex = index
          index += 1
          oldIndex
        }
      }
    }

  def toSeq: Vector[Double] = {
    val vb = Vector.newBuilder[Double]
    vb.sizeHint(numberOfPoints)
    (0 until numberOfPoints) foreach (index ⇒ vb.+=(start + (index * step)))
    vb.result()
  }
}