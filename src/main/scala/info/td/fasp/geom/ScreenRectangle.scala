package info.td.fasp.geom

abstract sealed class AbstractScreenRectangle

case object InvalidScreenRectangle extends AbstractScreenRectangle

case class ScreenRectangle(x1: Double, y1: Double, x2: Double, y2: Double) extends AbstractScreenRectangle {
  def left: Double = x1 min x2

  def right: Double = x1 max x2

  def top: Double = y1 min y2

  def bottom: Double = y1 max y2

  def width: Double = right - left

  def height: Double = bottom - top

  def horizontalCenter: Double = left + ((x1 - x2).abs / 2)

  def verticalCenter: Double = top + ((y1 - y2).abs / 2)

  def topLeft: ScreenPoint = new ScreenPoint(left, top)

  def topRight: ScreenPoint = new ScreenPoint(right, top)

  def bottomLeft: ScreenPoint = new ScreenPoint(left, bottom)

  def bottomRight: ScreenPoint = new ScreenPoint(right, bottom)

  def center: ScreenPoint = new ScreenPoint(horizontalCenter, verticalCenter)

  def horizontalRange: ScreenRange = ScreenRange(left, right)

  def verticalRange: ScreenRange = ScreenRange(top, bottom)

  def boundScreenPoint(screenPoint: ScreenPoint): ScreenPoint = {
    ScreenPoint(
      left max screenPoint.x min right,
      top max screenPoint.y min bottom
    )
  }

  def expanded(expandLeft: Double, expandTop: Double, expandRight: Double, expandBottom: Double): ScreenRectangle =
    ScreenRectangle(
      left - expandLeft,
      top - expandTop,
      right + expandRight,
      bottom + expandBottom
    )

  def union(other: ScreenRectangle): ScreenRectangle = {
    ScreenRectangle(
      left min other.left,
      top min other.top,
      right max other.right,
      bottom max other.bottom
    )
  }

  def intersection(other: ScreenRectangle): ScreenRectangle = {
    def intersectionMin(aMin: Double, aMax: Double, bMin: Double, bMax: Double): Double = {
      if (aMax < bMin || bMax < aMin)
        0 // no overlap at all
      else
        aMin max bMin
    }

    def intersectionMax(aMin: Double, aMax: Double, bMin: Double, bMax: Double): Double = {
      if (aMax < bMin || bMax < aMin)
        0 // no overlap at all
      else
        aMax min bMax
    }
    ScreenRectangle(
      intersectionMin(left, right, other.left, other.right),
      intersectionMin(top, bottom, other.top, other.bottom),
      intersectionMax(left, right, other.left, other.right),
      intersectionMax(top, bottom, other.top, other.bottom)
    )
  }

  def expanded(expandHorizontal: Double, expandVertical: Double): ScreenRectangle =
    ScreenRectangle(
      left - expandHorizontal,
      top - expandVertical,
      right + expandHorizontal,
      bottom + expandVertical
    )

  private def splitOffset(index: Int, total: Int, totalLength: Double): Int = {
    (index * totalLength / total).toInt
  }

  def horizontalSplit(index: Int, total: Int, span: Int = 1): ScreenRectangle =
    ScreenRectangle(
      splitOffset(index, total, width),
      top,
      splitOffset(index + span, total, width),
      bottom
    )

  def verticalSplit(index: Int, total: Int, span: Int = 1): ScreenRectangle =
    ScreenRectangle(
      left,
      splitOffset(index, total, height),
      right,
      splitOffset(index + span, total, height)
    )

  def isHorizontalHit(value: Double): Boolean = value >= left && value <= right

  def isVerticalHit(value: Double): Boolean = value >= top && value <= bottom

  def isHit(point: ScreenPoint): Boolean = isHorizontalHit(point.x) && isVerticalHit(point.y)

  def hasSameDimensionsAs(other: ScreenRectangle): Boolean =
    left == other.left && right == other.right && top == other.top && bottom == other.bottom

  override def toString = s"${this.getClass.getSimpleName}($left, $top, $right, $bottom)"
}

object ScreenRectangle {
  val zero = ScreenRectangle(0, 0, 0, 0)

  def apply(point1: ScreenPoint, point2: ScreenPoint): ScreenRectangle = ScreenRectangle(point1.x, point1.y, point2.x, point2.y)

  def topLeftBottomRight(top: Double, left: Double, bottom: Double, right: Double): ScreenRectangle =
    ScreenRectangle(left, top, right, bottom)

  def topLeftWidthHeight(top: Double, left: Double, width: Double, height: Double): ScreenRectangle =
    ScreenRectangle.topLeftBottomRight(top, left, top + height, left + width)
}