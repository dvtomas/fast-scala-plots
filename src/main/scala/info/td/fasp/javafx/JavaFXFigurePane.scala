package info.td.fasp.javafx

import javafx.scene.Node
import javafx.scene.canvas.Canvas
import javafx.scene.input.{InputEvent, KeyCode, KeyEvent, MouseEvent}
import javafx.scene.layout.Pane

import info.td.common.monix.MonixHelpers._
import info.td.common.monix.ProducerConsumerStrategy
import info.td.common.monix.javafx.JavaFXObservable
import info.td.fasp.controller._
import info.td.fasp.figurepart.FigurePart
import info.td.fasp.geom.ScreenPoint
import info.td.fasp.javafx.JavaFXConversions._
import info.td.fasp.javafx.figurepart.FigurePartRenderAction
import monifu.concurrent.Scheduler
import monifu.reactive.Observable

class ResizableCanvas extends Canvas {
  override def isResizable = true

  override def minWidth(height: Double) = 0

  override def minHeight(width: Double) = 0

  override def prefWidth(height: Double) = getWidth

  override def prefHeight(width: Double) = getHeight

  override def maxWidth(height: Double) = Double.MaxValue

  override def maxHeight(width: Double) = Double.MaxValue
}

private[this] class LRURenderActionProducerConsumerStrategy(scheduler: Scheduler) extends ProducerConsumerStrategy[FigurePartRenderAction, FigurePartRenderAction] {

  case class State(
                    pendingActions: Map[JavaFXRenderActionKey, FigurePartRenderAction],
                    lastRenderTimes: Map[JavaFXRenderActionKey, Long]
                  )

  override type StateType = State

  override def add(state: StateType, renderAction: FigurePartRenderAction): StateType = {
    state.copy(state.pendingActions.updated(renderAction.key, renderAction))
  }

  override def get(state: StateType): (Option[FigurePartRenderAction], StateType) = {
    if (state.pendingActions.isEmpty) {
      (None, state)
    } else {
      val lastRenderedActionKey = state.pendingActions.keys.minBy(key ⇒ state.lastRenderTimes.getOrElse(key, 0L))
      (
        Some(state.pendingActions(lastRenderedActionKey)),
        State(
          state.pendingActions - lastRenderedActionKey,
          state.lastRenderTimes.updated(lastRenderedActionKey, scheduler.now)
        )
        )
    }
  }

  override def empty: StateType = State(Map.empty, Map.empty)
}

class JavaFXFigurePane(renderActionFactory: RenderActionFactory) {
  private[this] val canvas = new ResizableCanvas

  lazy val pane: Pane = {
    val pane = new Pane
    canvas.widthProperty().bind(pane.widthProperty())
    canvas.heightProperty().bind(pane.heightProperty())
    pane
  }

  def subscribe(
                 partsSources: Observable[FigurePart],
                 overlayNodesSource: Observable[Seq[Node]]
               )(implicit scheduler: Scheduler): Unit = {
    import info.td.common.monix.MonixHelpers._

    partsSources
      .map(renderActionFactory.javaFXRenderAction)
      .producerConsumer(new LRURenderActionProducerConsumerStrategy(scheduler))
      .flatScan[Map[JavaFXRenderActionKey, FigurePartRenderAction.LastState]](Map.empty) {
      (lastStatesMap, renderAction) ⇒
        val lastState = lastStatesMap.getOrElse(renderAction.key, renderAction.initialState)
        renderAction.prepareRenderingOutsideJavaFXThread(lastState)
        Observable.fromFuture(
          JavaFXObservable.futureOnJavaFXApplicationThread {
            val newLastState = renderAction.render(canvas.getGraphicsContext2D, lastState)
            lastStatesMap.updated(renderAction.key, newLastState)
          }
        )
    }.subscribe()

    (Seq.empty +: overlayNodesSource)
      .subscribe(nodes ⇒
        JavaFXObservable.subscriptionOnJavaFXApplicationThread {
          import scala.collection.JavaConverters._
          pane.getChildren.setAll((Seq(canvas) ++ nodes).asJava)
        }
      )
  }

  def resizeEventsSource()(implicit scheduler: Scheduler): Observable[FigureEvent.Resize] = {
    Observable.defer(Observable.from(FigureEvent.Resize(canvas.getLayoutBounds.toScreenRectangle)))
      .++(
        JavaFXObservable.fromObservableValue(canvas.widthProperty())
          .combineLatest(JavaFXObservable.fromObservableValue(canvas.heightProperty()))
          .map { case (_, _) ⇒ FigureEvent.Resize(canvas.getLayoutBounds.toScreenRectangle) }
      )
  }

  def inputEventsSource()(implicit scheduler: Scheduler): Observable[FigureEvent] = {
    JavaFXObservable
      .fromNodeEvents(pane, InputEvent.ANY)
      .mergeMapIterable {
        case keyEvent: KeyEvent ⇒ {
          if (keyEvent.getEventType == KeyEvent.KEY_PRESSED && keyEvent.getCode == KeyCode.ESCAPE) {
            Some(FigureEvent.KeyPressed(Key.Escape))
          } else {
            None
          }
        }
        case mouseEvent: MouseEvent ⇒ {
          def modifiers(event: MouseEvent) = Key.Modifiers(event.isShiftDown, event.isControlDown, event.isAltDown)
          def screenPoint(event: MouseEvent) = ScreenPoint(event.getX, event.getY)

          if (mouseEvent.getEventType == MouseEvent.MOUSE_MOVED) {
            Some(FigureEvent.MouseMove(screenPoint(mouseEvent), modifiers(mouseEvent)))
          } else {
            val maybeButton = mouseEvent.getButton match {
              case javafx.scene.input.MouseButton.PRIMARY ⇒ Some(MouseButton.Primary)
              case javafx.scene.input.MouseButton.MIDDLE ⇒ Some(MouseButton.Middle)
              case javafx.scene.input.MouseButton.SECONDARY ⇒ Some(MouseButton.Secondary)
              case _ ⇒ None
            }
            maybeButton.flatMap { button ⇒
              mouseEvent.getEventType match {
                case MouseEvent.MOUSE_PRESSED ⇒ Some(FigureEvent.Click(button, screenPoint(mouseEvent), modifiers(mouseEvent)))
                case MouseEvent.MOUSE_DRAGGED ⇒ Some(FigureEvent.Drag(button, screenPoint(mouseEvent), modifiers(mouseEvent)))
                case MouseEvent.MOUSE_RELEASED ⇒ Some(FigureEvent.Release(button, screenPoint(mouseEvent), modifiers(mouseEvent)))
                case _ ⇒ None
              }
            }
          }
        }
        case scrollEvent: javafx.scene.input.ScrollEvent ⇒ Some(
          FigureEvent.Scroll(
            ScreenPoint(scrollEvent.getX, scrollEvent.getY),
            scrollEvent.getDeltaX, scrollEvent.getDeltaY,
            Key.Modifiers(scrollEvent.isShiftDown, scrollEvent.isControlDown, scrollEvent.isAltDown)
          )
        )
        case other ⇒ None
      }
  }

  def eventsSource()(implicit scheduler: Scheduler): Observable[FigureEvent] = {
    canvas.setFocusTraversable(true)
    canvas.addEventFilter(MouseEvent.ANY, (event: MouseEvent) ⇒ canvas.requestFocus())
    Observable.merge(resizeEventsSource(), inputEventsSource())
  }
}


