package info.td.fasp.javafx

import javafx.geometry.Bounds
import javafx.scene.canvas.GraphicsContext
import javafx.scene.text.{FontPosture, FontWeight}

import com.sun.javafx.tk.Toolkit
import info.td.fasp.geom.{ScreenPoint, ScreenRectangle}
import info.td.fasp.paint._

object JavaFXConversions {

  implicit class ColorExtensions(val color: info.td.fasp.paint.Color) extends AnyVal {
    def toJavaFX = new javafx.scene.paint.Color(color.red / 255.0, color.green / 255.0, color.blue / 255.0, 1.0)
  }

  implicit class BoundsExtensions(val bounds: Bounds) extends AnyVal {
    def toScreenRectangle = ScreenRectangle(bounds.getMinX, bounds.getMinY, bounds.getMaxX, bounds.getMaxY)
  }

  implicit class FontExtensions(val font: info.td.fasp.paint.FontProperties) extends AnyVal {
    def toJavaFX = {
      val (weight: FontWeight, posture: FontPosture) = font.style match {
        case info.td.fasp.paint.Font.Plain ⇒ (FontWeight.NORMAL, FontPosture.REGULAR)
        case info.td.fasp.paint.Font.Bold ⇒ (FontWeight.BOLD, FontPosture.REGULAR)
        case info.td.fasp.paint.Font.Italic ⇒ (FontWeight.NORMAL, FontPosture.ITALIC)
        case info.td.fasp.paint.Font.BoldItalic ⇒ (FontWeight.BOLD, FontPosture.ITALIC)
      }
      javafx.scene.text.Font.font(font.name, weight, posture, font.size)
    }
  }

  implicit class PointStyleExtension(val pointStyle: PointStyle) extends AnyVal {
    def draw(gc: GraphicsContext, x: Double, y: Double) = pointStyle match {
      case PointStyleNone ⇒
      case PointStyleRectangle(color) ⇒ {
        gc.setStroke(color.toJavaFX)
        gc.strokeRect(x - 1.0, y - 1.0, 2.0, 2.0)
      }
      case PointStyleDiamond(color, width) ⇒ {
        gc.setStroke(color.toJavaFX)
        gc.setLineWidth(width)
        gc.beginPath()
        gc.moveTo(x - 2.0, y)
        gc.lineTo(x, y - 2.0)
        gc.lineTo(x + 2.0, y)
        gc.lineTo(x, y + 2.0)
        gc.lineTo(x - 2.0, y)
        gc.stroke()
      }
    }

    def draw(gc: GraphicsContext, point: ScreenPoint): Unit = {
      draw(gc, point.x, point.y)
    }
  }

  implicit class LineStyleExtension(val lineStyle: LineStyle) extends AnyVal {
    def draw(gc: GraphicsContext, x1: Double, y1: Double, x2: Double, y2: Double) = lineStyle match {
      case LineStyleNone ⇒
      case LineStyleSimple(color, width) ⇒ {
        gc.setStroke(color.toJavaFX)
        gc.setLineWidth(width)
        gc.strokeLine(x1, y1, x2, y2)
      }
    }
  }

  implicit class ScreenRectangleExtensions(val screenRectangle: ScreenRectangle) extends AnyVal {
    def fill(gc: GraphicsContext, color: info.td.fasp.paint.Color): Unit = {
      gc.setFill(color.toJavaFX)
      gc.fillRect(screenRectangle.left, screenRectangle.top, screenRectangle.width, screenRectangle.height)
    }

    def stroke(gc: GraphicsContext, color: info.td.fasp.paint.Color, width: Double): Unit = {
      gc.setStroke(color.toJavaFX)
      gc.setLineWidth(width)
      gc.strokeRect(screenRectangle.left, screenRectangle.top, screenRectangle.width, screenRectangle.height)
    }

    /** Makes the rectangle be the clipping region for the GraphicsContext. Please note that an optimization for fast rectangular clipping as opposed to slow soft arbitrary-shaped clipping has been added probably as late as 8u40.
      *
      *
      * See:
      * http://mail.openjdk.java.net/pipermail/openjfx-dev/2014-May/014105.html
      * http://mail.openjdk.java.net/pipermail/openjfx-dev/2014-July/014678.html
      * Check if com.sun.javafx.geom.Path2D.checkAndGetIntRect exists, if so, the optimized version should probably be present.
      *
      */
    def fastClip(gc: GraphicsContext): Unit = {
      val left = screenRectangle.left.round // The coordinates must be rounded to the nearest integer within 1.0/256 precision.
      val right = screenRectangle.right.round
      val top = screenRectangle.top.round
      val bottom = screenRectangle.bottom.round

      gc.beginPath()
      gc.moveTo(left, top)
      gc.lineTo(right, top)
      gc.lineTo(right, bottom)
      gc.lineTo(left, bottom)
      gc.lineTo(left, top)
      gc.closePath()
      gc.clip()
    }
  }

  trait RichStringExtensionsHelpers extends Any {
    protected[this] def richString: RichString

    protected[this] def string: String = richString.string

    protected[this] def _draw(
                               g: GraphicsContext,
                               x: Double, y: Double,
                               horizontalAlignment: HorizontalAlignment, verticalAlignment: VerticalAlignment,
                               width: Double,
                               drawFunc: (GraphicsContext, Double, Double) ⇒ Unit
                             ): Unit = {
      if (!string.isEmpty) {
        val alignedX = horizontalAlignment match {
          case HorizontalAlignment.left ⇒ x
          case HorizontalAlignment.center ⇒ x - (width / 2)
          case HorizontalAlignment.right ⇒ x - width
        }
        val alignedY = verticalAlignment match {
          case VerticalAlignment.top ⇒ y + richString.height
          case VerticalAlignment.center ⇒ y + (richString.height / 2)
          case VerticalAlignment.bottom ⇒ y - (richString.height / 5)
        }
        drawFunc(g, alignedX, alignedY)
      }
    }
  }

  implicit class RichStringExtensions(val richString: RichString) extends AnyVal with RichStringExtensionsHelpers {

    def drawAt(g: GraphicsContext, x: Double, y: Double, horizontalAlignment: HorizontalAlignment, verticalAlignment: VerticalAlignment): Unit = {
      _draw(g,
        x, y,
        horizontalAlignment, verticalAlignment,
        Toolkit.getToolkit.getFontLoader.computeStringWidth(string, richString.properties.toJavaFX),
        (g, x, y) ⇒ {
          g.setFill(richString.properties.color.toJavaFX)
          g.setFont(richString.properties.toJavaFX)
          g.fillText(string, x, y)
        })
    }

    def drawAt(g: GraphicsContext, position: ScreenPoint, horizontalAlignment: HorizontalAlignment, verticalAlignment: VerticalAlignment): Unit = {
      drawAt(g, position.x, position.y, horizontalAlignment, verticalAlignment)
    }

    /** Renders the string roughly at position aligned in such a way, that it does not overlap the position, but is in the quadrant close to a certain lean point. */
    def drawAtAndLeaningTo(g: GraphicsContext, position: ScreenPoint, leanTo: ScreenPoint): Unit = {
      val (horizontalAlignment, xOffset) = if (position.x < leanTo.x) (HorizontalAlignment.left, 5) else (HorizontalAlignment.right, -5)
      val (verticalAlignment, yOffset) = if (position.y < leanTo.y) (VerticalAlignment.top, 5) else (VerticalAlignment.bottom, -5)
      drawAt(g, position.x + xOffset, position.y + yOffset, horizontalAlignment, verticalAlignment)
    }
  }

  implicit class ScientificNotationRichStringExtensions(val richString: RichString) extends AnyVal with RichStringExtensionsHelpers {
    private[this] def mantissaAndExponent = string.split('E') match {
      case Array(mantissa) ⇒ (mantissa, "")
      case Array(mantissa, exponent) ⇒ (if (mantissa == "1") "10" else mantissa + "×10", exponent.toInt.toString /* get rid of leading '+' */ )
    }

    private[this] def mantissa = mantissaAndExponent._1

    private[this] def exponent = mantissaAndExponent._2

    private[this] def mantissaFont = richString.properties.toJavaFX

    private[this] def exponentFont = {
      val font = richString.properties
      val newSize = Math.round(font.size * 0.9)
      font.copy(size = newSize).toJavaFX
    }

    private[this] def mantissaWidth = Toolkit.getToolkit.getFontLoader.computeStringWidth(mantissa, mantissaFont)

    private[this] def getWidth = {
      if (exponent.isEmpty) {
        mantissaWidth
      } else {
        mantissaWidth + 1 + Toolkit.getToolkit.getFontLoader.computeStringWidth(exponent, exponentFont)
      }
    }

    def scientificNotationDrawAt(g: GraphicsContext, x: Double, y: Double, horizontalAlignment: HorizontalAlignment, verticalAlignment: VerticalAlignment): Unit = {
      _draw(g,
        x, y,
        horizontalAlignment, verticalAlignment,
        getWidth,
        (g, x, y) ⇒ {
          g.setFill(richString.properties.color.toJavaFX)
          g.setFont(mantissaFont)
          g.fillText(mantissa, x, y)
          g.setFont(exponentFont)
          g.fillText(
            exponent,
            x.toInt + mantissaWidth + 1,
            (y - mantissaFont.getSize + exponentFont.getSize).toInt
          )
        })
    }

    def scientificNotationDrawAt(g: GraphicsContext, position: ScreenPoint, horizontalAlignment: HorizontalAlignment, verticalAlignment: VerticalAlignment): Unit = {
      scientificNotationDrawAt(g, position.x, position.y, horizontalAlignment, verticalAlignment)
    }

    /** Renders the string roughly at position aligned in such a way, that it does not overlap the position, but is in the quadrant close to a certain lean point. */
    def scientificNotationDrawAtAndLeaningTo(g: GraphicsContext, position: ScreenPoint, leanTo: ScreenPoint): Unit = {
      val (horizontalAlignment, xOffset) = if (position.x < leanTo.x) (HorizontalAlignment.left, 5) else (HorizontalAlignment.right, -5)
      val (verticalAlignment, yOffset) = if (position.y < leanTo.y) (VerticalAlignment.top, 5) else (VerticalAlignment.bottom, -5)
      scientificNotationDrawAt(g, position.x + xOffset, position.y + yOffset, horizontalAlignment, verticalAlignment)
    }
  }


  implicit class DoubleHelpers(val x: Double) extends AnyVal {
    // The 0.4999 instead of 0.5 so that rectangle fills and draws cover the same area with anti aliasing disabled..
    def snappedToPixelCenter: Double = x.floor + 0.4999
  }

}