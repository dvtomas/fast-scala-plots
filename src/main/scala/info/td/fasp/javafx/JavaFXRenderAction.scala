package info.td.fasp.javafx

import javafx.scene.canvas.GraphicsContext

case class JavaFXRenderActionKey(key: Symbol)

abstract class JavaFXRenderAction {
  /** For caching, actions can remember what has been rendered the last time in IntermediateState.
    * They are handed the last IntermediateState during rendering, and can use the information contained inside to skip rendering of some parts if the action decides that it has not changed since the last time.

    * The action is supposed to generate a new IntermediateState after the rendering. */
  type LastState

  def initialState: LastState

  /** Actions regarding the same item (plot) should have the same key. If there are more than one actions with the same key queued for rendering, only the most recent action will be rendered, the rest will be discarded. This way we don't have to render e. g. already obsolete versions of a plot when a newer version is available. */
  def key: JavaFXRenderActionKey

  /** Implement this for rendering actions that take a lot of resources to process, but at least some of the rendering can be performed outside the JavaFX application thread. Do any such rendering here, cache the result, and use it in the render() method later again. */
  def prepareRenderingOutsideJavaFXThread(lastState: LastState): Unit

  def render(gc: GraphicsContext, lastState: LastState): LastState
}