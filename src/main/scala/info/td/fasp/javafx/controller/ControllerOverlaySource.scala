package info.td.fasp.javafx.controller

import javafx.scene.Node
import javafx.scene.paint.{CycleMethod, LinearGradient, Stop}
import javafx.scene.shape.Rectangle

import info.td.fasp.controller.PlotController
import info.td.fasp.controller.PlotController.ZoomRectangleInProgress
import info.td.fasp.geom.ScreenRectangle
import monifu.reactive.Observable

object ControllerOverlaySource {
  def nodesFromControllerOutput(controllerOutputSource: Observable[Option[PlotController.State]]) = {
    def nodes(state: PlotController.State): Seq[Node] = state match {
      case ZoomRectangleInProgress(origin, corner) ⇒ {
        val screenRectangle = ScreenRectangle(origin, corner)
        val rectangle = new Rectangle(screenRectangle.left + 0.5, screenRectangle.top + 0.5, screenRectangle.width, screenRectangle.height)
        val opacity = 0.3

        rectangle.setStroke(new javafx.scene.paint.Color(0.2, 0.2, 0.2, opacity))
        val backgroundStops = Seq(
          new Stop(0.0, new javafx.scene.paint.Color(0.7, 0.7, 1.0, opacity)),
          new Stop(1.0, new javafx.scene.paint.Color(0.5, 0.5, 1.0, opacity))
        )
        val background = new LinearGradient(0, 0, 1, 1, true, CycleMethod.NO_CYCLE, backgroundStops: _*)
        rectangle.setFill(background)
        rectangle.setMouseTransparent(true)
        Seq(rectangle)
      }
      case _ ⇒ Seq.empty
    }

    Observable.concat(
      Observable(Seq.empty),
      controllerOutputSource map (_.map(nodes).getOrElse(Seq.empty))
    )
  }
}