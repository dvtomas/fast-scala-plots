package info.td.fasp.javafx

import java.awt.Graphics2D

import com.typesafe.scalalogging.LazyLogging
import info.td.fasp.figurepart._
import info.td.fasp.geom.ScreenPoint
import info.td.fasp.javafx.figurepart.FigurePartRenderAction
import info.td.fasp.paint._
import info.td.fasp.plot.{Plot, XYPlot}
import info.td.fasp.swing.AwtConversions
import info.td.fasp.swing.plot.{AbstractPlotRenderer, XYPlotRenderer}
import info.td.fasp.utils.ValueFormatterLocale

class PlotRenderer extends AbstractPlotRenderer with LazyLogging {
  def render(g: Graphics2D, plot: Plot, axes: Axes): Unit = plot match {
    case plot: XYPlot ⇒ XYPlotRenderer.render(g, plot, axes)
    case other ⇒ {
      val errorString = s"Don't know how to render plot ${other.getClass.getName}"
      logger.warn(errorString)
      AwtConversions.RichStringExtensions(new RichString(errorString, FontProperties(color = Color.red, style = Font.Bold)))
        .drawAt(g, ScreenPoint(1, 1), HorizontalAlignment.left, VerticalAlignment.top)
    }
  }
}

class RenderActionFactory(implicit valueFormatterLocale: ValueFormatterLocale) extends LazyLogging {

  protected[this] def plotRenderer = new PlotRenderer

  def javaFXRenderAction(state: FigurePart): FigurePartRenderAction =
    FigurePartRenderAction(state, plotRenderer)
}