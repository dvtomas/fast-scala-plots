package info.td.fasp.javafx.figurepart

import java.awt.image.BufferedImage
import javafx.embed.swing.SwingFXUtils
import javafx.scene.canvas.GraphicsContext

import info.td.fasp.axispainter.{LinearAxisPainterX, LinearAxisPainterY, NullAxisPainterX, NullAxisPainterY}
import info.td.fasp.figurepart.FigurePart
import info.td.fasp.javafx.JavaFXConversions._
import info.td.fasp.javafx.axis.{AxisRendererHelpers, LinearAxisRenderer}
import info.td.fasp.javafx.{JavaFXRenderAction, JavaFXRenderActionKey}
import info.td.fasp.paint.{HorizontalAlignment, VerticalAlignment}
import info.td.fasp.swing.AwtConversions
import info.td.fasp.swing.plot.AbstractPlotRenderer
import info.td.fasp.swing.utils.Graphics2DUtils
import info.td.fasp.utils.ValueFormatterLocale

object FigurePartRenderAction {

  case class LastState()

}

case class FigurePartRenderAction(
                                   figurePart: FigurePart,
                                   plotRenderer: AbstractPlotRenderer
                                 )(implicit valueFormatterLocale: ValueFormatterLocale)
  extends JavaFXRenderAction {
  /* Actions regarding the same item (plot) should have the same key. If there are more than one actions with the same key queued for rendering, only the most recent action will be rendered, the rest will be discarded. This way we don't have to render e. g. already obsolete versions of a plot when a newer version is available. */
  override def key = JavaFXRenderActionKey(figurePart.key.key)

  type LastState = FigurePartRenderAction.LastState

  def initialState = new LastState

  def screenBounds = figurePart.screenBounds

  private[this] def axesScreenBounds = figurePart.axesScreenBounds

  override def render(gc: GraphicsContext, lastState: LastState): LastState = {
    figurePart.screenBounds.fill(gc, figurePart.properties.backgroundColor)

    val borderWidth = 1.0

    figurePart.screenBounds.expanded(borderWidth / -2, borderWidth / -2)
      .stroke(gc, figurePart.properties.borderColor, borderWidth)

    axesScreenBounds.expanded(borderWidth / 2, borderWidth / 2)
      .stroke(gc, figurePart.properties.axesBorderColor, borderWidth)

    renderViewport(gc)
    renderAxisX(gc)
    renderAxisY(gc)

    // Plot title
    figurePart.properties.title.drawAt(gc,
      figurePart.screenBounds.horizontalCenter,
      figurePart.screenBounds.top + 2, HorizontalAlignment.center, VerticalAlignment.top
    )

    // Label X
    figurePart.properties.xLabel.drawAt(gc,
      figurePart.screenBounds.horizontalCenter,
      figurePart.screenBounds.bottom - 2, HorizontalAlignment.center, VerticalAlignment.bottom
    )

    // Label Y
    val oldTransform = gc.getTransform
    gc.translate(figurePart.screenBounds.left, axesScreenBounds.verticalCenter)
    gc.rotate(-90)
    figurePart.properties.yLabel.drawAt(gc, figurePart.screenBounds.left, figurePart.screenBounds.left, HorizontalAlignment.center, VerticalAlignment.top)
    gc.setTransform(oldTransform)

    lastState
  }

  private[this] def renderAxisX(gc: GraphicsContext)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    figurePart.properties.axisPainterX match {
      case painter: LinearAxisPainterX ⇒ (new LinearAxisRenderer).renderLinearAxisX(gc, figurePart.axes, painter)
      case NullAxisPainterX ⇒
      case other ⇒ AxisRendererHelpers.reportAxisXPaintError(gc, s"Don't have a renderer for X axis $other", figurePart.axes)
    }
  }

  private[this] def renderAxisY(gc: GraphicsContext)(implicit valueFormatterLocale: ValueFormatterLocale): Unit = {
    figurePart.properties.axisPainterY match {
      case painter: LinearAxisPainterY ⇒ (new LinearAxisRenderer).renderLinearAxisY(gc, figurePart.axes, painter)
      case NullAxisPainterY ⇒
      case other ⇒ AxisRendererHelpers.reportAxisXPaintError(gc, s"Don't have a renderer for Y axis $other", figurePart.axes)
    }
  }

  private[this] lazy val writableImage = {
    val width = axesScreenBounds.width.toInt
    val height = axesScreenBounds.height.toInt
    val awtImage = if (width > 0 && height > 0) {
      //      val image = Graphics2DUtils.createCompatibleImage(width, height)
      // I've found the type 3 to be slightly faster than createCompatibleImage (creates type 1), we avoid creating a new BufferedImage in the SwingFXUtils.toFXImage call
      val image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB_PRE)
      val g = image.createGraphics()
      Graphics2DUtils.setAntiAliasedRendering(g, figurePart.properties.antiAliasedRendering)
      g.setBackground(AwtConversions.ColorExtensions(figurePart.properties.backgroundColor).toAwt)
      g.clearRect(0, 0, image.getWidth, image.getHeight)
      g.translate(-axesScreenBounds.left.toInt, -axesScreenBounds.top.toInt)

      figurePart.plots.foreach(plot ⇒ plotRenderer.render(g, plot, figurePart.axes))
      image
    } else {
      Graphics2DUtils.createCompatibleImage(1, 1)
    }

    SwingFXUtils.toFXImage(awtImage, null)
  }

  override def prepareRenderingOutsideJavaFXThread(lastState: LastState) = {
    /* touch */ writableImage
  }

  private[this] def renderViewport(gc: GraphicsContext): Unit = {
    gc.drawImage(writableImage, axesScreenBounds.left, axesScreenBounds.top)
  }
}
