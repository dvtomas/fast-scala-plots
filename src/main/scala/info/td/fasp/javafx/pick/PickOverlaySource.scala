package info.td.fasp.javafx.pick

import javafx.scene.paint.Color
import javafx.scene.shape.{Circle, Line, Rectangle}
import javafx.scene.text.Text
import javafx.scene.{Group, Node, Scene}

import com.typesafe.scalalogging.LazyLogging
import info.td.fasp.figure.Figure
import info.td.fasp.figurepart.FigurePart
import info.td.fasp.javafx.JavaFXConversions.{DoubleHelpers, _}
import info.td.fasp.pick._
import info.td.fasp.plot.LinePlot
import info.td.fasp.utils.ValueFormatterLocale
import monifu.reactive.Observable

abstract class PickOverlaySource {
  protected[this] def nodes(pickResult: PickResult, figure: Figure): Seq[Node]

  def nodesFromPickResults(
                            figureSource: Observable[Figure],
                            pickResultsSource: Observable[Seq[PickResult]]
                          ): Observable[Seq[Node]] =
    Observable.concat(
      Observable(Seq.empty),
      (pickResultsSource combineLatest figureSource) map {
        case (pickResults, figurePart) ⇒ pickResults.flatMap(pickResult ⇒ nodes(pickResult, figurePart))
      }
    )

  protected[this] def pickerLine(x1: Double, y1: Double, x2: Double, y2: Double) = {
    val line = new Line(x1, y1, x2, y2)
    line.setStroke(new javafx.scene.paint.Color(0.2, 0.2, 0.2, 0.75))
    line
  }

  protected[this] def pointPicks(pickResult: PickResult, figurePart: FigurePart)(implicit valueFormatterLocale: ValueFormatterLocale): Seq[Node] = {
    pickResult.pickedPoints.flatMap {
      case XYPickedPoint(plotKey, pointIndex) ⇒ {
        figurePart.plots
          .find(_.plotKey == plotKey)
          .toSeq
          .flatMap { plot ⇒
            plot match {
              case linePlot: LinePlot ⇒ {
                linePlot.data.maybePoint(pointIndex).toSeq.flatMap { dataPoint ⇒
                  val screenPoint = figurePart.axes.screenPoint(dataPoint).snappedToPixelCenter

                  val color = linePlot.linePlottingProperties.pickColor.toJavaFX
                  val fillColor = color.deriveColor(0, 1.0, 20.0, 0.8)
                  // http://stackoverflow.com/questions/3942878/how-to-decide-font-color-in-white-or-black-depending-on-background-color
                  val textColor =
                    if (fillColor.getRed * 0.299 + fillColor.getGreen * 0.587 + fillColor.getBlue * 0.114 > 0.5) {
                      Color.BLACK
                    } else {
                      Color.WHITE
                    }
                  val strokeColor = fillColor.deriveColor(0, 1.0, 0.5, 1.0)

                  val pointCircle = new Circle(screenPoint.x, screenPoint.y, 3)
                  pointCircle.setStroke(strokeColor)
                  pointCircle.setFill(fillColor)

                  val descriptionText = linePlot.pickDescriber.describe(linePlot, pointIndex)
                  val maybeDescriptionNodes =
                    if (descriptionText.nonEmpty) {
                      val horizontalPadding = 3
                      val verticalPadding = 3

                      // http://stackoverflow.com/questions/13015698/how-to-calculate-the-pixel-width-of-a-string-in-javafx
                      val text = new Text(descriptionText)
                      new Scene(new Group(text))
                      text.applyCss()
                      val (textWidth, textHeight) = (text.getLayoutBounds.getWidth, text.getLayoutBounds.getHeight)
                      val width = (textWidth + 2 * horizontalPadding).toInt
                      val height = (textHeight + 2 * verticalPadding).toInt

                      val gravityPoint = figurePart.axesOnScreen.axesScreenBounds.center
                      val x =
                        (if (screenPoint.x < gravityPoint.x) {
                          screenPoint.x
                        } else {
                          screenPoint.x - width
                        }).snappedToPixelCenter

                      val y =
                        (if (screenPoint.y < gravityPoint.y) {
                          screenPoint.y
                        } else {
                          screenPoint.y - height
                        }).snappedToPixelCenter

                      val backgroundRectangle = new Rectangle(width, height)
                      backgroundRectangle.setX(x)
                      backgroundRectangle.setY(y)
                      text.setX(x + horizontalPadding)
                      text.setY(y + verticalPadding + text.getBaselineOffset)

                      backgroundRectangle.setStroke(strokeColor)
                      backgroundRectangle.setFill(fillColor)
                      backgroundRectangle.setArcWidth(5.0)
                      backgroundRectangle.setArcHeight(5.0)

                      text.setFill(textColor)

                      Seq(backgroundRectangle, text)
                    } else {
                      Seq.empty
                    }
                  Seq(pointCircle) ++ maybeDescriptionNodes
                }
              }
              case _ ⇒ None
            }
          }
      }
      case _ ⇒ Seq.empty
    }
  }
}

class NonePickOverlaySource extends PickOverlaySource {
  override protected[this] def nodes(pickResult: PickResult, figure: Figure): Seq[Node] =
    Seq.empty
}

class XYPickOverlaySource(implicit valueFormatterLocale: ValueFormatterLocale) extends PickOverlaySource {
  override protected[this] def nodes(pickResult: PickResult, figure: Figure): Seq[Node] = {
    figure.figureParts.flatMap { figurePart ⇒
      val axes = figurePart.axes
      val bounds = axes.screenBounds
      pickResult.pickerPositions
        .filter(_ ⇒ pickResult.figurePartKey == figurePart.key)
        .flatMap { pickerPosition ⇒
          val x = axes.axisX.screenValue(pickerPosition.dataPoint.x).snappedToPixelCenter
          val maybeXLine = if (bounds.isHorizontalHit(x)) {
            Seq(pickerLine(x, bounds.top, x, bounds.bottom))
          } else {
            Seq.empty
          }

          val y = axes.axisY.screenValue(pickerPosition.dataPoint.y).snappedToPixelCenter
          val maybeYLine = if (bounds.isVerticalHit(y)) {
            Seq(pickerLine(bounds.left, y, bounds.right, y))
          } else {
            Seq.empty
          }

          maybeXLine ++ maybeYLine
        } ++ pointPicks(pickResult, figurePart)
    }
  }
}

class VerticalPickOverlaySource(implicit valueFormatterLocale: ValueFormatterLocale) extends PickOverlaySource {
  override protected[this] def nodes(pickResult: PickResult, figure: Figure): Seq[Node] = {
    figure.figureParts.flatMap { figurePart ⇒
      val axes = figurePart.axes
      val bounds = axes.screenBounds
      pickResult.pickerPositions
        .filter(_ ⇒ pickResult.figurePartKey == figurePart.key)
        .flatMap { pickerPosition ⇒
          val x = axes.axisX.screenValue(pickerPosition.dataPoint.x).snappedToPixelCenter
          if (bounds.isHorizontalHit(x)) {
            Seq(pickerLine(x, bounds.top, x, bounds.bottom))
          } else {
            Seq.empty
          }
        } ++ pointPicks(pickResult, figurePart)
    }
  }
}

object PickOverlaySource extends LazyLogging {
  def none = new NonePickOverlaySource

  def forStrategy(strategy: PickStrategy)(implicit valueFormatterLocale: ValueFormatterLocale) = strategy match {
    case PickStrategy.None ⇒ new NonePickOverlaySource
    case PickStrategy.Vertical ⇒ new VerticalPickOverlaySource
    case PickStrategy.XY ⇒ new XYPickOverlaySource
    case _ ⇒ {
      logger.error(s"Unknown PickStrategy: $strategy")
      new NonePickOverlaySource
    }
  }

  def forPicker(picker: FigurePartPicker)(implicit valueFormatterLocale: ValueFormatterLocale) = forStrategy(picker.strategy)
}