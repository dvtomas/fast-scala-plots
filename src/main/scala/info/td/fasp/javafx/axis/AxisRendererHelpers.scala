package info.td.fasp.javafx.axis

import javafx.scene.canvas.GraphicsContext

import com.typesafe.scalalogging.LazyLogging
import info.td.fasp.axis.{LinearAxisX, LinearAxisY}
import info.td.fasp.axispainter._
import info.td.fasp.figurepart.Axes
import info.td.fasp.geom.ScreenPoint
import info.td.fasp.javafx.JavaFXConversions._
import info.td.fasp.paint._
import info.td.fasp.utils.ValueFormatterLocale

object AxisRendererHelpers extends LazyLogging {
  private[this] def paintBottomAndTop(g: GraphicsContext, tickSettings: TicksSettings, bottom: Double, top: Double, x: Double) {
    val (snappedBottom, snappedTop, snappedX) = (bottom.snappedToPixelCenter, top.snappedToPixelCenter, x.snappedToPixelCenter)
    g.setStroke(tickSettings.color.toJavaFX)
    g.setLineWidth(1)
    g.strokeLine(snappedX, snappedBottom - 1, snappedX, snappedBottom - tickSettings.tickLength)
    g.strokeLine(snappedX, snappedTop, snappedX, snappedTop + tickSettings.tickLength - 1)
  }

  private[this] def paintLeftAndRight(g: GraphicsContext, tickSettings: TicksSettings, left: Double, right: Double, y: Double) {
    val (snappedLeft, snappedRight, snappedY) = (left.snappedToPixelCenter, right.snappedToPixelCenter, y.snappedToPixelCenter)
    g.setStroke(tickSettings.color.toJavaFX)
    g.setLineWidth(1)

    g.strokeLine(snappedLeft, snappedY, snappedLeft + tickSettings.tickLength - 1, snappedY)
    g.strokeLine(snappedRight - tickSettings.tickLength, snappedY, snappedRight - 1, snappedY)
  }

  def paintMinorTicksOnHorizontalAxes(g: GraphicsContext, bottom: Double, top: Double, ticksPlacement: TicksPlacement, ticksSettings: TicksSettings) {
    for (tickPlacement ← ticksPlacement.ticks) {
      paintBottomAndTop(g, ticksSettings, bottom, top, tickPlacement.screenValue)
    }
  }

  def paintMajorTicksOnHorizontalAxes(g: GraphicsContext, bottom: Double, top: Double, ticksPlacement: TicksPlacement, properties: AxisPaintingProperties)
                                     (implicit valueFormatterLocale: ValueFormatterLocale) {
    val ticksSettings = properties.majorTicksSettings
    for (tickPlacement ← ticksPlacement.ticks) {
      paintBottomAndTop(g, ticksSettings, bottom, top, tickPlacement.screenValue)
      val digits = ticksPlacement.positionOfDigitThatChanges max 2
      val valueString = properties.valueString(tickPlacement.dataValue, digits)
      new RichString(valueString, properties.fontProperties).scientificNotationDrawAt(g, tickPlacement.screenValue, bottom, HorizontalAlignment.center, VerticalAlignment.top)
    }
  }

  def paintMinorTicksOnVerticalAxes(g: GraphicsContext, left: Double, right: Double, ticksPlacement: TicksPlacement, ticksSettings: TicksSettings) {
    for (tickPlacement ← ticksPlacement.ticks) {
      paintLeftAndRight(g, ticksSettings, left, right, tickPlacement.screenValue)
    }
  }

  def paintMajorTicksOnVerticalAxes(g: GraphicsContext, left: Double, right: Double, ticksPlacement: TicksPlacement, properties: AxisPaintingProperties)
                                   (implicit valueFormatterLocale: ValueFormatterLocale) {
    val ticksSettings = properties.majorTicksSettings
    for (tickPlacement ← ticksPlacement.ticks) {
      paintLeftAndRight(g, ticksSettings, left, right, tickPlacement.screenValue)
      val digits = ticksPlacement.positionOfDigitThatChanges max 2
      val valueString = properties.valueString(tickPlacement.dataValue, digits)
      new RichString(valueString, properties.fontProperties).scientificNotationDrawAt(g, left - 3, tickPlacement.screenValue, HorizontalAlignment.right, VerticalAlignment.center)
    }
  }

  private[this] def reportAxisPaintError(g: GraphicsContext, error: String, position: ScreenPoint): Unit = {
    g.setFill(Color.red.toJavaFX)
    g.setFont(FontProperties().toJavaFX)
    g.fillText(error, position.x, position.y)
    logger.error(error)
  }

  def reportAxisXPaintError(g: GraphicsContext, error: String, axes: Axes): Unit = {
    reportAxisPaintError(g, error, ScreenPoint(axes.axisX.screenRange.minimum, axes.axisY.screenRange.maximum))
  }

  def reportAxisYPaintError(g: GraphicsContext, error: String, axes: Axes): Unit = {
    reportAxisPaintError(g, error, ScreenPoint(axes.axisX.screenRange.minimum, axes.axisY.screenRange.center))
  }
}

class LinearAxisRenderer {
  def renderLinearAxisX(g: GraphicsContext, axes: Axes, axisPainterX: LinearAxisPainterX)
                       (implicit valueFormatterLocale: ValueFormatterLocale) {
    val axisX = axes.axisX
    val (bottom, top) = (axes.axisY.screenRange.maximum, axes.axisY.screenRange.minimum)
    val minorTicksSettings = axisPainterX.properties.minorTicksSettings
    val majorTicksSettings = axisPainterX.properties.majorTicksSettings
    axisX match {
      case linearAxisX: LinearAxisX ⇒ {
        AxisRendererHelpers.paintMinorTicksOnHorizontalAxes(g, bottom, top, minorTicksSettings.computeTicksPlacement(axisX), minorTicksSettings)
        AxisRendererHelpers.paintMajorTicksOnHorizontalAxes(g, bottom, top, majorTicksSettings.computeTicksPlacement(axisX), axisPainterX.properties)
      }
      case other ⇒ AxisRendererHelpers.reportAxisXPaintError(g, s"Can't use linear axis renderer for a non-linear axis $other", axes)
    }
  }

  def renderLinearAxisY(g: GraphicsContext, axes: Axes, axisPainterY: LinearAxisPainterY)
                       (implicit valueFormatterLocale: ValueFormatterLocale) {
    val axisY = axes.axisY
    val (left, right) = (axes.axisX.screenRange.minimum, axes.axisX.screenRange.maximum)
    val minorTicksSettings = axisPainterY.properties.minorTicksSettings
    val majorTicksSettings = axisPainterY.properties.majorTicksSettings
    axisY match {
      case linearAxisY: LinearAxisY ⇒ {
        AxisRendererHelpers.paintMinorTicksOnVerticalAxes(g, left, right, minorTicksSettings.computeTicksPlacement(axisY), minorTicksSettings)
        AxisRendererHelpers.paintMajorTicksOnVerticalAxes(g, left, right, majorTicksSettings.computeTicksPlacement(axisY), axisPainterY.properties)
      }
      case other ⇒ AxisRendererHelpers.reportAxisXPaintError(g, s"Can't use linear axis renderer for a non-linear axis $other", axes)
    }
  }
}