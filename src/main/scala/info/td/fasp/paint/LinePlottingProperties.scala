package info.td.fasp.paint

trait HasColor {
  def color: Color
}

// LineStyle

abstract sealed class LineStyle

object LineStyleNone extends LineStyle {
  def color = Color.black
}

case class LineStyleSimple(color: Color, width: Double = 1.0) extends LineStyle with HasColor

// PointStyle

abstract sealed class PointStyle

object PointStyleNone extends PointStyle

case class PointStyleRectangle(color: Color) extends PointStyle with HasColor

case class PointStyleDiamond(color: Color, width: Double = 2.0) extends PointStyle with HasColor

// LinePlottingProperties

case class LinePlottingProperties(
                                   lineStyle: LineStyle = LineStyleSimple(Color.gray),
                                   pointStyle: PointStyle = PointStyleRectangle(Color.black)
                                 ) {
  def pickColor: Color = {
    pointStyle match {
      case hasColor: HasColor ⇒ hasColor.color
      case _ ⇒ lineStyle match {
        case hasColor: HasColor ⇒ hasColor.color
        case _ ⇒ Color.black
      }
    }
  }
}
