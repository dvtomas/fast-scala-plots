package info.td.fasp.paint

case class Color(red: Int, green: Int, blue: Int)

case object Color {
  val white = Color(255, 255, 255)
  val lightGray = Color(192, 192, 192)
  val gray = Color(192, 192, 192)
  val darkGray = Color(64, 64, 64)
  val black = Color(0, 0, 0)
  val red = Color(255, 0, 0)
  val pink = Color(255, 175, 175)
  val orange = Color(255, 200, 0)
  val yellow = Color(255, 255, 0)
  val green = Color(0, 255, 0)
  val magenta = Color(255, 0, 255)
  val cyan = Color(0, 255, 255)
  val blue = Color(0, 0, 255)
}