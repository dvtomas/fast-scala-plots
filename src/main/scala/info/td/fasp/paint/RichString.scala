package info.td.fasp.paint

case class RichString(string: String, properties: FontProperties = FontProperties()) {
  def height = properties.size
}