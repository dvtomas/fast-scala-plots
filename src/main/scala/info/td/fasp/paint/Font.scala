package info.td.fasp.paint

object Font {

  abstract sealed class FontStyle

  case object Plain extends FontStyle

  case object Bold extends FontStyle

  case object Italic extends FontStyle

  case object BoldItalic extends FontStyle
}

case class FontProperties(
                           size: Double = 12,
                           style: Font.FontStyle = Font.Plain,
                           name: String = "Sans-Serif",
                           color: Color = Color.black
                         )