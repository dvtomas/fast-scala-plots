package info.td.fasp.utils

import java.util.Locale

case class ValueFormatterLocale(locale: Locale)
