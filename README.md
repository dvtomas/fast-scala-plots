# Fast scala plots

This library features a JavaFX component for plotting data completely implemented in the Scala programming language. Primarily, it is being developed for a commercial automotive measuring and automation project, but I have decided to also publish it for a wider audience.  However, with the tragic death of my boss (Libor, I'll always remember you), the future of this library is uncertain. If you want to follow up with the development, I'll be happy to help, as I think the infrastructure is nicely implemented, and there has been a lot of thinking and experience put into the source code.

This library is a complete rework of the [fast-scala-plots-old](https://bitbucket.org/dvtomas/fast-scala-plots-old/) library. Please, download the [demo of the original library](https://bitbucket.org/dvtomas/fast-scala-plots-old/downloads) to see where this library is aiming at. However, this library is not as feature complete as the original one yet. The main differences (and motivations for a new version) are

 - Rendering is decoupled from processing - the original library was tied to Swing, this one is primarily being developed for JavaFX, however, Swing or other targets could be easily implemented
 - That goes hand in hand with multi-touch support - that was impossible to do in Swing, but should be easy with JavaFX
 - FRP implementation from the ground up using the awesome [Monix library](https://github.com/monixio/monix)

The examples require Java 8 to run. Grab the [latest examples in the Downloads section](https://bitbucket.org/dvtomas/fast-scala-plots/downloads). The archive contains windows executable wrapper and a linux shell script to run the demo.

## Design goals and features

 - Allow fast display of growing data (new data are acquired in real-time and the user wants to see them ASAP) even on slower (embedded) machines.
 - Focus mainly on line plots and height maps. Don't add dozens of different graph types (yet), keep it simple. For line plots, the library should support XY plots and function plots with equidistant X step (aka _snakes_).
 - At least for snakes, allow display of very large data (millions of points).
 - Look nice.
 - Low memory footprint. Just use getters to retrieve the points, don't force the user to wrap the points into some specific structure.

## Main features

 - Executable demo
 - XY graphs with configurable point/line styles
 - Mouse control: LMB draws a zoom rectangle, MMB drags, RMB pops up a menu, wheel zooms in/out
 - Vertical/XY picker of data
 - Speed optimized and memory-canny. All point collections are virtual, so memory use can be very low. I have seen plotting libraries where each point had to be an instance of Point2D(x, y) **and** a Publisher, that is definitely not the case of this library.

## Not implemented yet, but already implemented in fast-scala-plots-old, so easy to port

 - Convenient user interface for both desktop computers and touch devices.
 - Proper support for localization, right now the library is localized to English and Czech.
 - Speed optimized function plots with equidistant X step (_snakes_)
 - Height map
 - Touch device control: Toolbar buttons change mode of operation (zoom, drag, pick, ...)
 - Legend
 - Scroll-bars navigation support
 - User can choose between antialiased rendering and high performance rendering.
 - Copy image to clipboard, save image to file features
 - Fit to screen, Fit Y to screen features
 - Linear/Logarithmic/Mirrored logarithmic axes
 - Trampoline control (see demo) for moving in very long graphs

## What will probably not be implemented

 - Polar axes/picker
 - Allow several plots in a figure to overlap (but you can have several lines/points/etc in one plot, don't worry)

